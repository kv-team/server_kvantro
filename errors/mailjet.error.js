/**
 * Обработка ошибок связанных с сервисом MailJet
 */
import * as utils from '../utils';
import logger from '../services/logger';
import {stats} from '../services/statsd';

export class MailjetError extends Error {
    constructor(...args) {
        super(...args);
        this.message = args[0].ErrorMessage;
        Error.captureStackTrace(this, MailjetError);
    }
}

export function MailjetErrorHandler(err, req, res, next) {
    if (err instanceof MailjetError) {
        logger.error(err.message);
        stats.increment('statsd.mailjet.error');
        res.status(400).json(utils.formatResponse(req, 400, null, err.message));
    } else {
        next(err);
    }
}
