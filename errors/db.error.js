import logger from '../services/logger';
import {Error} from '../constants';
import {stats} from '../services/statsd';

export default function DbErrorHandler(error) {

    switch (error.original.code) {
        case Error.ECONNREFUSED: {
            stats.increment(`statsd.${Error.ECONNREFUSED}`);
            logger.error(`${error.message}`);
            process.exit(1);
            break;
        }
        default: {
            logger.error(`${error.message}`);
        }
    }
}
