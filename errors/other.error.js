import logger from '../services/logger';
import {config} from '../config';
import {Error} from '../constants';

const finder = require('process-finder');

export default function errorHandler(error) {

    if (error.syscall !== 'listen') {
        throw error;
    }

    switch (error.code) {
        case Error.EACCESS:
            logger.warn('Server pipe requires elevated privileges');
            process.exit(1);
            break;
        case Error.EADDRINUSE:
            finder.find(config.get('port'), function (err, pids) {

                if (err) {
                    logger.error(err.message);
                    process.exit(1);
                    return;
                }

                pids.forEach(function (pid) {
                    process.kill(pid);
                    logger.warn(`PORT: ${config.get('port')} close active process: ${pid}`);
                });

                process.exit();
            });
            break;
        case Error.SIGQUIT:
            logger.warn(`PORT: ${config.get('port')} close process with sigquit`);
            process.exit();
            break;
        case Error.SIGTERM:
            logger.warn(`PORT: ${config.get('port')} close process with sigterm`);
            process.exit();
            break;
        default:
            throw error;
    }
};