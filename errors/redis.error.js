import logger from '../services/logger';
import {stats} from '../services/statsd';

export default function RedisErrorHandler(error) {
    stats.increment('statsd.redis.error');
    logger.error(`${error.message}`);
}
