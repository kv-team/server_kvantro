import logger from '../services/logger';
import ValidationError from './validation.error';
import {MailjetErrorHandler} from './mailjet.error';
import * as utils from '../utils';
import AuthError from './auth.error';

export default (core) => {
    core.app.use(AuthError);
    core.app.use(ValidationError);
    core.app.use(MailjetErrorHandler);
    core.app.use((err, req, res) => {
        let code = err.status || (err.original && err.original.code) ? err.original.code : 500;
        res.status(code).json(utils.formatResponse(req, code, null, err));
        logger.error(err);
    });
};
