import * as utils from '../utils';
import {ErrorDB} from '../constants';
import {stats} from '../services/statsd';

export default function validationErrorHandler(err, req, res, next) {

    switch (err.name) {
        case ErrorDB.VALIDATION: {
            stats.increment(`statsd.${ErrorDB.VALIDATION}`);
            res.status(400).json(utils.formatResponse(req, 400, null, err.errors));
            break;
        }
        case ErrorDB.FK_CONSTRAINTS: {
            stats.increment(`statsd.${ErrorDB.FK_CONSTRAINTS}`);
            res.status(400).json(utils.formatResponse(req, 400, null, [{message: 'Verify the correctness of the data'}]));
            break;
        }
        case ErrorDB.DATABASE: {
            stats.increment(`statsd.${ErrorDB.DATABASE}`);
            res.status(400).json(utils.formatResponse(req, 400, null, [{message: 'Verify the correctness of the data'}]));
            break;
        }
        case ErrorDB.UNIQUE_CONSTRAINT: {
            stats.increment(`statsd.${ErrorDB.UNIQUE_CONSTRAINT}`);
            res.status(400).json(utils.formatResponse(req, 400, null, [{message: 'Verify the correctness of the data'}]));
            break;
        }
        default: {
            next(err);
        }
    }
};
