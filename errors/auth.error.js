import * as utils from '../utils';
import {Error} from '../constants';

export default function AuthError(err, req, res, next) {
    if (err.name === Error.AUTHENTICATION) {
        res.status(err.status).json(utils.formatResponse(req, err.status, null, err.message));
    } else {
        next(err);
    }
};
