import readline from 'readline-sync';
import DbService from '../services/database';

const dbService = new DbService();

namespace('db', () => {

    desc('deploy database');
    task('deploy', async () => {
        try {
            console.log(`Start deploy database =================================`.bold.cyan);
            await dbService.deployDB();
            console.log('Finished deploy database =============================='.bold.cyan);
            complete();
        } catch (err) {
            console.log('Stop deploy database =================================='.bold.cyan);
            fail(err);
        }
    }, true);

    desc('recreate database');
    task('recreate', async () => {
        try {

            if (!readline.keyInYN('Do you really want to run the task?'.bold.yellow)) {
                console.log('Cancel recreate'.green);
                complete();
            }

            console.log(`Start recreate database ================================`.bold.cyan);
            await dbService.recreateDB();
            console.log('Finished recreate database ============================='.bold.cyan);
            complete();
        } catch (err) {
            console.log('Stop recreate database ================================='.bold.cyan);
            fail(err);
        }
    }, true);

    desc('revert database');
    task('revert', async () => {
        try {

            if (!readline.keyInYN('Do you really want to run the task?'.bold.yellow)) {
                console.log('Cancel revert'.green);
                complete();
            }

            console.log(`Start revert ===================================`.bold.cyan);
            await dbService.seedRevert();
            console.log("Step 1/2: ".bold.yellow + "Seeds revert complete\r".italic.green);
            await dbService.migrateRevert();
            console.log("Step 2/2: ".bold.yellow + "Migrate revert complete\r".italic.green);

            console.log('Finished revert ================================'.bold.cyan);

            complete();
        } catch (err) {
            fail(err);
        }
    }, true);
});