import * as utils from '../utils';
import Helpers from '../helpers';

namespace('gen', () => {

    utils.Generator.generateCountLimit(100, 'Users', async () => {await Helpers.tests.addUser()});

    /** API V1 *******************************************/

    utils.Generator.generateCount('Clients', async () => {await Helpers.tests.addClient()});

    utils.Generator.generateCount('Skills', async () => {await Helpers.tests.addSkill()});

    utils.Generator.generateCount('Categories', async () => {await Helpers.tests.addCategory()});

    utils.Generator.generateCount('Tags', async () => {await Helpers.tests.addTag()});

    utils.Generator.generateCount('Shares', async () => {await Helpers.tests.addShare()});

    /* region Works */
    utils.Generator.generateCount('Works', async () => {await Helpers.tests.addWork()});

    utils.Generator.generateCount('Works_tags', async () => {await Helpers.tests.addWorkTag()});

    utils.Generator.generateCount('Works_shares', async () => {await Helpers.tests.addWorkShare()});

    utils.Generator.generateCount('Works_skills', async () => {await Helpers.tests.addWorkSkill()});

    utils.Generator.generateCount('Works_comments', async () => {await Helpers.tests.addWorkComment()});

    utils.Generator.generateCount('Works_categories', async () => {await Helpers.tests.addWorkCategory()});
    /* endregion */

    /* region Articles */
    utils.Generator.generateCount('Articles', async () => {await Helpers.tests.addArticle()});

    utils.Generator.generateCount('Articles_tags', async () => {await Helpers.tests.addArticleTag()});

    utils.Generator.generateCount('Articles_shares', async () => {await Helpers.tests.addArticleShare()});

    utils.Generator.generateCount('Articles_categories', async () => {await Helpers.tests.addArticleCategory()});

    utils.Generator.generateCount('Articles_comments', async () => {await Helpers.tests.addArticleComment()});

    utils.Generator.generateCount('Messages', async () => {await Helpers.tests.addMessage()});
    /* endregion */
});