import ip from 'ip';
import {config} from '../config';
import DbService from '../services/database';
import AppService from '../services/app';

let appService = new AppService(config),
    dbService = new DbService(config);

namespace('default', () => {

    desc('Start application');
    task('dev', async () => {
        try {
            console.log(`Start deploy application [${config.dev}] =====================`.bold.cyan);

            await dbService.deployDB();
            console.log('Step 1/3: '.bold.yellow + 'Database deploy complete\r'.italic.green);
            //await appService.testStart();
            console.log('Step 2/3: '.bold.yellow + 'Testing complete\r'.italic.green);
            //await appService.demonStart();
            console.log('Step 3/3: '.bold.yellow + 'Demon start complete\r'.italic.green);

            console.log('------------------------------------'.gray);
            console.log('     Local: '.white + (`http://${config.get('host')}:${config.get('port')}`).gray);
            console.log('  External: '.white + (`http://${ip.address()}:${config.get('port')}`).gray);
            console.log('------------------------------------'.gray);

            complete();
            console.log('Finished deploy ================================'.bold.cyan);
        } catch (err) {
            console.log('Stop deploy ===================================='.bold.cyan);
            fail(err);
        }
    }, true);

    desc('generate data application');
    task('generate', [
        'gen:users[10]',
        'gen:works[100]',
        'gen:articles[100]',
    ], () => {});
});