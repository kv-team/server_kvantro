import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.SharesController();

describe.only('Testing controller Shares', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getShares', () => {
        it.only('Existence of the method get shares', done => {
            expect(ctrl).to.have.property('getShares');
            done();
        });
    });

    describe.only('method getShare', () => {
        it.only('Existence of the method get share', done => {
            expect(ctrl).to.have.property('getShare');
            done();
        });
    });

    describe.only('method addShare', () => {
        it.only('Existence of the method add share', done => {
            expect(ctrl).to.have.property('addShare');
            done();
        });
    });

    describe.only('method patchShare', () => {
        it.only('Existence of the method patch share', done => {
            expect(ctrl).to.have.property('patchShare');
            done();
        });
    });

    describe.only('method delShare', () => {
        it.only('Existence of the method del share', done => {
            expect(ctrl).to.have.property('delShare');
            done();
        });
    });
});