import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.ArticlesSharesController();

describe.only('Testing controller Articles shares', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getArticleSharesByArticleId', () => {
        it.only('Existence of the method get article shares by article id', done => {
            expect(ctrl).to.have.property('getArticleSharesByArticleId');
            done();
        });
    });

    describe.only('method addArticleShare', () => {
        it.only('Existence of the method add articles share', done => {
            expect(ctrl).to.have.property('addArticleShare');
            done();
        });
    });

    describe.only('method delArticleShare', () => {
        it.only('Existence of the method del articles share', done => {
            expect(ctrl).to.have.property('delArticleShare');
            done();
        });
    });
});