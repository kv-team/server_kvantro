import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.SkillsController();

describe.only('Testing controller Skills', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getSkills', () => {
        it.only('Existence of the method get skills', done => {
            expect(ctrl).to.have.property('getSkills');
            done();
        });
    });

    describe.only('method getSkill', () => {
        it.only('Existence of the method get skill', done => {
            expect(ctrl).to.have.property('getSkill');
            done();
        });
    });

    describe.only('method addSkill', () => {
        it.only('Existence of the method add skill', done => {
            expect(ctrl).to.have.property('addSkill');
            done();
        });
    });

    describe.only('method patchSkill', () => {
        it.only('Existence of the method patch skill', done => {
            expect(ctrl).to.have.property('patchSkill');
            done();
        });
    });

    describe.only('method delSkill', () => {
        it.only('Existence of the method del skill', done => {
            expect(ctrl).to.have.property('delSkill');
            done();
        });
    });
});