import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.CategoriesController();

describe.only('Testing controller Categories', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getCategories', () => {
        it.only('Existence of the method get categorys', done => {
            expect(ctrl).to.have.property('getCategories');
            done();
        });
    });

    describe.only('method getCategory', () => {
        it.only('Existence of the method get category', done => {
            expect(ctrl).to.have.property('getCategory');
            done();
        });
    });

    describe.only('method addCategory', () => {
        it.only('Existence of the method add category', done => {
            expect(ctrl).to.have.property('addCategory');
            done();
        });
    });

    describe.only('method patchCategory', () => {
        it.only('Existence of the method patch category', done => {
            expect(ctrl).to.have.property('patchCategory');
            done();
        });
    });

    describe.only('method delCategory', () => {
        it.only('Existence of the method del category', done => {
            expect(ctrl).to.have.property('delCategory');
            done();
        });
    });
});