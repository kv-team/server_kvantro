import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.ArticlesCommentsController();

describe.only('Testing controller Articles comments', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getArticleCommentsByArticleId', () => {
        it.only('Existence of the method get article comments by article id', done => {
            expect(ctrl).to.have.property('getArticleCommentsByArticleId');
            done();
        });
    });

    describe.only('method addArticleComment', () => {
        it.only('Existence of the method add article comment', done => {
            expect(ctrl).to.have.property('addArticleComment');
            done();
        });
    });

    describe.only('method delArticleComment', () => {
        it.only('Existence of the method del article comment', done => {
            expect(ctrl).to.have.property('delArticleComment');
            done();
        });
    });
});