import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.WorksCategoriesController();

describe.only('Testing controller Works categories', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getWorkCategoriesByWorkId', () => {
        it.only('Existence of the method get work categories by work id', done => {
            expect(ctrl).to.have.property('getWorkCategoriesByWorkId');
            done();
        });
    });

    describe.only('method addWorkCategory', () => {
        it.only('Existence of the method add work category', done => {
            expect(ctrl).to.have.property('addWorkCategory');
            done();
        });
    });

    describe.only('method delWorkCategory', () => {
        it.only('Existence of the method del work category', done => {
            expect(ctrl).to.have.property('delWorkCategory');
            done();
        });
    });
});