import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.WorksTagsController();

describe.only('Testing controller Works tags', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getWorkTagsByWorkId', () => {
        it.only('Existence of the method get work tags by work id', done => {
            expect(ctrl).to.have.property('getWorkTagsByWorkId');
            done();
        });
    });

    describe.only('method addWorkTag', () => {
        it.only('Existence of the method add works tag', done => {
            expect(ctrl).to.have.property('addWorkTag');
            done();
        });
    });

    describe.only('method delWorkTag', () => {
        it.only('Existence of the method del works tag', done => {
            expect(ctrl).to.have.property('delWorkTag');
            done();
        });
    });
});