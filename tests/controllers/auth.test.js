import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.AuthController();

describe.only('Testing controller Auth', () => {
    
    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getArticle', () => {
        it.only('Existence of the method is authorizated', done => {
            expect(ctrl).to.have.property('isAuthorizated');
            done();
        });
    });

    describe.only('method logOut', () => {
        it.only('Existence of the method log out', done => {
            expect(ctrl).to.have.property('logOut');
            done();
        });
    });

    describe.only('method logIn', () => {
        it.only('Existence of the method log in', done => {
            expect(ctrl).to.have.property('logIn');
            done();
        });
    });

    describe.only('method resetPswEmail', () => {
        it.only('Existence of the method reset psw', done => {
            expect(ctrl).to.have.property('resetPswEmail');
            done();
        });
    });

    describe.only('method deserializeUser', () => {
        it.only('Existence of the method deserialize user', done => {
            expect(ctrl).to.have.property('deserializeUser');
            done();
        });
    });

    describe.only('method serializeUser', () => {
        it.only('Existence of the method serialize user', done => {
            expect(ctrl).to.have.property('serializeUser');
            done();
        });
    });
});