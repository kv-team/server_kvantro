import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.TagsController();

describe.only('Testing controller Tags', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getTags', () => {
        it.only('Existence of the method get tags', done => {
            expect(ctrl).to.have.property('getTags');
            done();
        });
    });

    describe.only('method getTag', () => {
        it.only('Existence of the method get tag', done => {
            expect(ctrl).to.have.property('getTag');
            done();
        });
    });

    describe.only('method addTag', () => {
        it.only('Existence of the method add tag', done => {
            expect(ctrl).to.have.property('addTag');
            done();
        });
    });

    describe.only('method patchTag', () => {
        it.only('Existence of the method patch tag', done => {
            expect(ctrl).to.have.property('patchTag');
            done();
        });
    });

    describe.only('method delTag', () => {
        it.only('Existence of the method del tag', done => {
            expect(ctrl).to.have.property('delTag');
            done();
        });
    });
});