import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.UsersController();

describe.only('Testing controller Users', () => {
    

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getUsers', () => {
        it.only('Existence of the method get users', done => {
            expect(ctrl).to.have.property('getUsers');
            done();
        });
    });

    describe.only('method getUser', () => {
        it.only('Existence of the method get user', done => {
            expect(ctrl).to.have.property('getUser');
            done();
        });
    });

    describe.only('method addUser', () => {
        it.only('Existence of the method add user', done => {
            expect(ctrl).to.have.property('addUser');
            done();
        });
    });

    describe.only('method patchUser', () => {
        it.only('Existence of the method patch user', done => {
            expect(ctrl).to.have.property('patchUser');
            done();
        });
    });

    describe.only('method delUser', () => {
        it.only('Existence of the method delete user', done => {
            expect(ctrl).to.have.property('delUser');
            done();
        });
    });

    describe.only('method getRestoreToken', () => {
        it.only('Existence of the method reset password user', done => {
            expect(ctrl).to.have.property('getRestoreToken');
            done();
        });
    });

    describe.only('method resetPswConfirmUser', () => {
        it.only('Existence of the method reset password confirmation user', done => {
            expect(ctrl).to.have.property('resetPswConfirmUser');
            done();
        });
    });
});