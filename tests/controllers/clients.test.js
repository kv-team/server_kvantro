import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.ClientsController();

describe.only('Testing controller Clients', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getClients', () => {
        it.only('Existence of the method get clients', done => {
            expect(ctrl).to.have.property('getClients');
            done();
        });
    });

    describe.only('method getClient', () => {
        it.only('Existence of the method get client', done => {
            expect(ctrl).to.have.property('getClient');
            done();
        });
    });

    describe.only('method addClient', () => {
        it.only('Existence of the method add client', done => {
            expect(ctrl).to.have.property('addClient');
            done();
        });
    });

    describe.only('method patchClient', () => {
        it.only('Existence of the method patch client', done => {
            expect(ctrl).to.have.property('patchClient');
            done();
        });
    });

    describe.only('method delClient', () => {
        it.only('Existence of the method del client', done => {
            expect(ctrl).to.have.property('delClient');
            done();
        });
    });
});