import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.WorksCommentsController();

describe.only('Testing controller Works comments', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getWorkCommentsByWorkId', () => {
        it.only('Existence of the method get work comments by work id', done => {
            expect(ctrl).to.have.property('getWorkCommentsByWorkId');
            done();
        });
    });

    describe.only('method addWorkComment', () => {
        it.only('Existence of the method add work comment', done => {
            expect(ctrl).to.have.property('addWorkComment');
            done();
        });
    });

    describe.only('method delWorkComment', () => {
        it.only('Existence of the method del work comment', done => {
            expect(ctrl).to.have.property('delWorkComment');
            done();
        });
    });
});