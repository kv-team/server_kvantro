import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.WorksSkillsController();

describe.only('Testing controller Works skills', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getWorkSkillsByWorkId', () => {
        it.only('Existence of the method get work skills by work id', done => {
            expect(ctrl).to.have.property('getWorkSkillsByWorkId');
            done();
        });
    });

    describe.only('method addWorkSkill', () => {
        it.only('Existence of the method add works skill', done => {
            expect(ctrl).to.have.property('addWorkSkill');
            done();
        });
    });

    describe.only('method delWorkSkill', () => {
        it.only('Existence of the method del works skill', done => {
            expect(ctrl).to.have.property('delWorkSkill');
            done();
        });
    });
});