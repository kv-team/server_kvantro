import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.ArticlesController();

describe.only('Testing controller Articles', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getArticles', () => {
        it.only('Existence of the method get articles', done => {
            expect(ctrl).to.have.property('getArticles');
            done();
        });
    });

    describe.only('method getArticle', () => {
        it.only('Existence of the method get article', done => {
            expect(ctrl).to.have.property('getArticle');
            done();
        });
    });

    describe.only('method addArticle', () => {
        it.only('Existence of the method add article', done => {
            expect(ctrl).to.have.property('addArticle');
            done();
        });
    });

    describe.only('method patchArticle', () => {
        it.only('Existence of the method patch article', done => {
            expect(ctrl).to.have.property('patchArticle');
            done();
        });
    });

    describe.only('method delArticle', () => {
        it.only('Existence of the method del article', done => {
            expect(ctrl).to.have.property('delArticle');
            done();
        });
    });
});