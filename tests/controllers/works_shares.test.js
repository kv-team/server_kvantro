import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.WorksSharesController();

describe.only('Testing controller Works shares', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getWorkSharesByWorkId', () => {
        it.only('Existence of the method get work shares by article id', done => {
            expect(ctrl).to.have.property('getWorkSharesByWorkId');
            done();
        });
    });

    describe.only('method addWorkShare', () => {
        it.only('Existence of the method add works share', done => {
            expect(ctrl).to.have.property('addWorkShare');
            done();
        });
    });

    describe.only('method delWorkShare', () => {
        it.only('Existence of the method del works share', done => {
            expect(ctrl).to.have.property('delWorkShare');
            done();
        });
    });
});