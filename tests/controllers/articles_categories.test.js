import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.ArticlesCategoriesController();

describe.only('Testing controller Articles categories', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getArticleCategoriesByArticleId', () => {
        it.only('Existence of the method get article categories by article id', done => {
            expect(ctrl).to.have.property('getArticleCategoriesByArticleId');
            done();
        });
    });

    describe.only('method addArticleCategory', () => {
        it.only('Existence of the method add article category', done => {
            expect(ctrl).to.have.property('addArticleCategory');
            done();
        });
    });

    describe.only('method delArticleCategory', () => {
        it.only('Existence of the method del article category', done => {
            expect(ctrl).to.have.property('delArticleCategory');
            done();
        });
    });
});