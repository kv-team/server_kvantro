import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.MessagesController();

describe.only('Testing controller Messages', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getMessages', () => {
        it.only('Existence of the method get messages', done => {
            expect(ctrl).to.have.property('getMessages');
            done();
        });
    });

    describe.only('method getMessage', () => {
        it.only('Existence of the method get message', done => {
            expect(ctrl).to.have.property('getMessage');
            done();
        });
    });

    describe.only('method addMessage', () => {
        it.only('Existence of the method add message', done => {
            expect(ctrl).to.have.property('addMessage');
            done();
        });
    });

    describe.only('method patchMessage', () => {
        it.only('Existence of the method patch message', done => {
            expect(ctrl).to.have.property('patchMessage');
            done();
        });
    });

    describe.only('method delMessage', () => {
        it.only('Existence of the method del message', done => {
            expect(ctrl).to.have.property('delMessage');
            done();
        });
    });
});