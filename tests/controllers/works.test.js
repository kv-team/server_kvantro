import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.WorksController();

describe.only('Testing controller Works', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getWorks', () => {
        it.only('Existence of the method get works', done => {
            expect(ctrl).to.have.property('getWorks');
            done();
        });
    });

    describe.only('method getWork', () => {
        it.only('Existence of the method get work', done => {
            expect(ctrl).to.have.property('getWork');
            done();
        });
    });

    describe.only('method addWork', () => {
        it.only('Existence of the method add work', done => {
            expect(ctrl).to.have.property('addWork');
            done();
        });
    });

    describe.only('method patchWork', () => {
        it.only('Existence of the method patch work', done => {
            expect(ctrl).to.have.property('patchWork');
            done();
        });
    });

    describe.only('method delWork', () => {
        it.only('Existence of the method del work', done => {
            expect(ctrl).to.have.property('delWork');
            done();
        });
    });
});