import chai from 'chai';
import Controllers from '../../controllers';

const expect = chai['expect'];
const ctrl = new Controllers.v1.ArticlesTagsController();

describe.only('Testing controller Articles tags', () => {

    describe.only('Properties', () => {
        it.only('Existence private properties', done => {
            expect(ctrl).to.have.property('_t');
            expect(ctrl).to.have.property('_core');
            done();
        });
    });

    describe.only('method getArticleTagsByArticleId', () => {
        it.only('Existence of the method get article tags by article id', done => {
            expect(ctrl).to.have.property('getArticleTagsByArticleId');
            done();
        });
    });

    describe.only('method addArticleTag', () => {
        it.only('Existence of the method add articles tag', done => {
            expect(ctrl).to.have.property('addArticleTag');
            done();
        });
    });

    describe.only('method delArticleTag', () => {
        it.only('Existence of the method del articles tag', done => {
            expect(ctrl).to.have.property('delArticleTag');
            done();
        });
    });
});