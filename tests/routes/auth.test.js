import chai from 'chai';
import chaiHttp from 'chai-http';
import Helpers from '../../helpers';
import {serverHttp} from '../../server';

chai.use(chaiHttp);
const agent = chai.request.agent(serverHttp);
const expect = chai.expect;

let adminUser;

describe.only('Testing route auth', () => {

    before(done => {
        Helpers.tests.logIn(agent).then(user => {
            adminUser = user;
            done();
        }).catch(done);
    });

    it.only('GET /auth/login', done => {
        chai.request(serverHttp)
            .get('/auth/login')
            .end((err, res) => {
                if (err) return done(err);
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('data').to.be.an('object');
                done();
            });
    });

    it.only('GET /auth/logout', done => {
        agent
            .get('/auth/logout')
            .end((err, res) => {
                if (err) return done(err);
                expect(res).to.have.status(200);
                expect(res.redirects.length).to.equal(1);
                expect(res.redirects[0]).to.have.string('/auth/login');
                done();
            });
    });

    it.only('POST /auth/restore', done => {
        agent
            .post('/auth/restore')
            .send({username: adminUser.user_email})
            .end((err, res) => {
                if (err) return done(err);
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('data').to.be.an('object');
                done();
            });
    });

    after(done => {
        serverHttp.close();
        done();
    });
});