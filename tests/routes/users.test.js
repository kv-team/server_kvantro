import chai from 'chai';
import chaiHttp from 'chai-http';
import Helpers from '../../helpers';
import faker from 'faker';
import {serverHttp} from '../../server';
import {HttpMethods} from '../../constants';

chai.use(chaiHttp);

const expect = chai.expect;
const agent = chai.request.agent(serverHttp);

let userAdmin, _token, _randomUser;


describe.only('Testing route users', () => {

    describe.only('Test user login in', () => {

        beforeEach(done => {
            Helpers.tests.logIn(agent).then((user) => {
                userAdmin = user;
                done();
            }).catch(done);
        });

        describe.only('route /users/:user_id', () => {

            it.only(HttpMethods.GET, done => {
                agent
                    .get(`/users/${userAdmin.user_id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /users', () => {

            it.only(HttpMethods.GET, done => {
                agent
                    .get('/users')
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {
                const first_name = faker.name.firstName(),
                    email = faker.internet.email();
                agent.post('/users')
                    .send({psw_two: '134', psw_one: '134', first_name, email})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(201);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('user_name').to.equal(first_name);
                        expect(res.body.data).to.have.property('user_email').to.equal(email);
                        expect(res.body.data).to.have.property('user_social').to.have.property('local').to.have.property('psw');
                        expect(res.body.data).to.have.property('user_social').to.have.property('local').to.have.property('jwt');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {
                const username = faker.name.firstName(),
                    email = faker.internet.email();
                agent.patch('/users')
                    .send({username, email})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('user_name').to.equal(username);
                        expect(res.body.data).to.have.property('user_email').to.equal(email);
                        expect(res.body.data).to.have.property('user_id').to.equal(userAdmin.user_id);
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {

                agent
                    .delete('/users')
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });
        });
    });

    describe.only('Test user login out', () => {

        beforeEach(done => {
            Helpers.tests.getRandomUser().then(user => {
                _randomUser = user;
                done();
            }).catch(done);
        });

        describe.only('route /users/:id', () => {

            it.only(HttpMethods.GET, done => {

                chai.request(serverHttp)
                    .get(`/users/${userAdmin.user_id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });
        });

        describe.only('route /users', () => {

            it.only(HttpMethods.GET, done => {
                chai.request(serverHttp)
                    .get('/users')
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {
                const first_name = faker.name.firstName(),
                    email = faker.internet.email();
                chai.request(serverHttp)
                    .post('/users')
                    .send({psw_two: '134', psw_one: '134', first_name, email})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(201);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('user_name').to.equal(first_name);
                        expect(res.body.data).to.have.property('user_email').to.equal(email);
                        expect(res.body.data).to.have.property('user_social').to.have.property('local').to.have.property('psw');
                        expect(res.body.data).to.have.property('user_social').to.have.property('local').to.have.property('jwt');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {

                chai.request(serverHttp)
                    .patch('/users')
                    .send({username: faker.name.firstName()})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {
                chai.request(serverHttp)
                    .delete('/users')
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });
        });
    });

    describe.only('route /users/restore', () => {

        beforeEach(done => {
            Helpers.tests.getRandomConfirm().then(token => {
                _token = token;
                done();
            }).catch(done);
        });

        it.only('GET invalid token', done => {
            chai.request(serverHttp)
                .get('/users/restore')
                .query({token: faker.random.uuid()})
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res).to.have.status(200);
                    expect(res.redirects.length).to.equal(1);
                    expect(res.redirects[0]).to.have.string('auth/login');
                    done();
                });
        });

        it.only('GET valid token', done => {
            chai.request(serverHttp)
                .get('/users/restore')
                .query({token: _token.confirm_token})
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('data').to.be.an('object');
                    expect(res.body.data).to.have.property('token').to.equal(_token.confirm_token);
                    done();
                });
        });

        it.only('POST valid password', done => {
            chai.request(serverHttp)
                .post('/users/restore')
                .send({psw_one: '123', psw_two: '123', token: _token.confirm_token})
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res).to.have.status(200);
                    expect(res.redirects.length).to.equal(1);
                    expect(res.redirects[0]).to.have.string('auth/login');
                    done();
                });
        });

        it.only('POST invalid password', done => {
            chai.request(serverHttp)
                .post('/users/restore')
                .send({psw_one: '123', psw_two: '1234', token: _token.confirm_token})
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an('object');
                    expect(res.body).to.have.property('errors').to.be.an('array');
                    done();
                });
        });
    });

    after(done => {
        serverHttp.close();
        done();
    });
});