import chai from 'chai';
import chaiHttp from 'chai-http';
import Helpers from '../../../helpers';
import faker from 'faker';
import {serverHttp} from '../../../server';
import {tryCatch} from '../../../decorators';
import {HttpMethods} from '../../../constants';

chai.use(chaiHttp);

const basePath = '/api/v1/messages';
const expect = chai.expect;
const agent = chai.request.agent(serverHttp);

let userAdmin, randomMessage;

describe.only('Testing route messages', () => {

    describe.only('Test user login in', () => {

        beforeEach(tryCatch(async () => {
            userAdmin = await Helpers.tests.logIn(agent);
            randomMessage = await Helpers.tests.getRandomMessage();
        }));

        describe.only('route /messages/:id', () => {

            it.only(HttpMethods.GET, done => {

                agent.get(`${basePath}/${randomMessage.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {

                const message_name = faker.name.firstName(),
                    message_email = faker.internet.email(),
                    message_body = faker.lorem.sentence();

                agent.patch(`${basePath}/${randomMessage.id}`)
                    .send({message_email, message_name, message_body})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('message_name').to.equal(message_name);
                        expect(res.body.data).to.have.property('message_email').to.equal(message_email);
                        expect(res.body.data).to.have.property('message_body').to.equal(message_body);
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {

                agent.delete(`${basePath}/${randomMessage.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /messages', () => {

            it.only(HttpMethods.GET, done => {

                agent.get(basePath)
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {

                const message_name = faker.name.firstName(),
                    message_email = faker.internet.email(),
                    message_body = faker.lorem.sentence();

                agent.post(basePath)
                    .send({message_email, message_name, message_body})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(201);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('message_name').to.equal(message_name);
                        expect(res.body.data).to.have.property('message_email').to.equal(message_email);
                        expect(res.body.data).to.have.property('message_body').to.equal(message_body);
                        done();
                    });
            });
        });
    });

    describe.only('Test user login out', () => {

        beforeEach(done => {
            Helpers.tests.getRandomMessage().then(data => {
                randomMessage = data;
                done();
            }).catch(done);
        });

        describe.only('route /messages/:id', () => {

            it.only(HttpMethods.GET, done => {

                chai.request(serverHttp)
                    .get(`${basePath}/${randomMessage.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {

                const message_name = faker.name.firstName(),
                    message_email = faker.internet.email(),
                    message_body = faker.lorem.paragraph();

                chai.request(serverHttp)
                    .patch(`${basePath}/${randomMessage.id}`)
                    .send({message_email, message_name, message_body})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {
                chai.request(serverHttp)
                    .delete(`${basePath}/${randomMessage.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /messages', () => {

            it.only(HttpMethods.GET, done => {
                chai.request(serverHttp)
                    .get(basePath)
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {

                const message_name = faker.name.firstName(),
                    message_email = faker.internet.email(),
                    message_body = faker.lorem.paragraph();

                chai.request(serverHttp)
                    .post(basePath)
                    .send({message_email, message_name, message_body})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });
        });
    });

    after(done => {
        serverHttp.close();
        done();
    });
});