import chai from 'chai';
import chaiHttp from 'chai-http';
import Helpers from '../../../helpers';
import faker from 'faker';
import {serverHttp} from '../../../server';
import {tryCatch} from '../../../decorators';
import {HttpMethods} from '../../../constants';

chai.use(chaiHttp);

const basePath = '/api/v1/clients';
const agent = chai.request.agent(serverHttp);
const expect = chai.expect;

let userAdmin, randomClient;

describe.only('Testing route clients', () => {

    describe.only('Test user login in', () => {

        beforeEach(tryCatch(async () => {
            userAdmin = await Helpers.tests.logIn(agent);
            randomClient = await Helpers.tests.getRandomClient();
        }));

        describe.only('route /clients/:id', () => {

            it.only(HttpMethods.GET, done => {

                agent.get(`${basePath}/${randomClient.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {

                const client_name = faker.lorem.sentence();

                agent.patch(`${basePath}/${randomClient.id}`)
                    .send({client_name})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('client_name').to.equal(client_name);
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {

                agent.delete(`${basePath}/${randomClient.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /clients', () => {

            it.only(HttpMethods.GET, done => {

                agent.get(basePath)
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {

                const client_name = faker.lorem.sentence();

                agent.post(basePath)
                    .send({client_name})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(201);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('client_name').to.equal(client_name);
                        done();
                    });
            });
        });
    });

    describe.only('Test user login out', () => {

        beforeEach(done => {
            Helpers.tests.getRandomClient().then(data => {
                randomClient = data;
                done();
            }).catch(done);
        });

        describe.only('route /clients/:id', () => {

            it.only(HttpMethods.GET, done => {

                chai.request(serverHttp)
                    .get(`${basePath}/${randomClient.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {
                const client_name = faker.lorem.sentence();
                chai.request(serverHttp)
                    .patch(`${basePath}/${randomClient.id}`)
                    .send({client_name})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {
                chai.request(serverHttp)
                    .delete(`${basePath}/${randomClient.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /clients', () => {

            it.only(HttpMethods.GET, done => {
                chai.request(serverHttp)
                    .get(basePath)
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {
                const client_name = faker.lorem.sentence();
                chai.request(serverHttp)
                    .post(basePath)
                    .send({client_name})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });
        });
    });

    after(done => {
        serverHttp.close();
        done();
    });
});