import chai from 'chai';
import chaiHttp from 'chai-http';
import Helpers from '../../../helpers';
import faker from 'faker';
import {serverHttp} from '../../../server';
import {tryCatch} from '../../../decorators';
import {HttpMethods} from '../../../constants';

chai.use(chaiHttp);

const basePath = '/api/v1/tags';
const expect = chai.expect;
const agent = chai.request.agent(serverHttp);

let userAdmin, randomTag;

describe.only('Testing route tags', () => {

    describe.only('Test user login in', () => {

        beforeEach(tryCatch(async () => {
            userAdmin = await Helpers.tests.logIn(agent);
            randomTag = await Helpers.tests.getRandomTag();
        }));

        describe.only('route /tags/:id', () => {

            it.only(HttpMethods.GET, done => {

                agent.get(`${basePath}/${randomTag.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {

                const tag_name = faker.lorem.sentence();

                agent.patch(`${basePath}/${randomTag.id}`)
                    .send({tag_name})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('tag_name').to.equal(tag_name);
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {

                agent.delete(`${basePath}/${randomTag.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /tags', () => {

            it.only(HttpMethods.GET, done => {

                agent.get(basePath)
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {

                const tag_name = faker.lorem.sentence();

                agent.post(basePath)
                    .send({tag_name})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(201);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('tag_name').to.equal(tag_name);
                        done();
                    });
            });
        });

    });

    describe.only('Test user login out', () => {

        beforeEach(done => {
            Helpers.tests.getRandomTag().then(data => {
                randomTag = data;
                done();
            }).catch(done);
        });

        describe.only('route /tags/:id', () => {

            it.only(HttpMethods.GET, done => {

                chai.request(serverHttp)
                    .get(`${basePath}/${randomTag.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {

                const tag_name = faker.lorem.sentence();

                chai.request(serverHttp)
                    .patch(`${basePath}/${randomTag.id}`)
                    .send({tag_name})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {

                chai.request(serverHttp)
                    .delete(`${basePath}/${randomTag.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /tags', () => {

            it.only(HttpMethods.GET, done => {

                chai.request(serverHttp)
                    .get(basePath)
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {

                const tag_name = faker.lorem.sentence();

                chai.request(serverHttp)
                    .post(basePath)
                    .send({tag_name})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });
        });

    });

    after(done => {
        serverHttp.close();
        done();
    });
});