import chai from 'chai';
import chaiHttp from 'chai-http';
import Helpers from '../../../helpers';
import moment from 'moment';
import faker from 'faker';
import {serverHttp} from '../../../server';
import {tryCatch} from '../../../decorators';
import {HttpMethods} from '../../../constants';

chai.use(chaiHttp);

const basePath = '/api/v1/works';
const expect = chai.expect;
const agent = chai.request.agent(serverHttp);

describe.only('Testing route works', () => {

    describe.only('Test user login in', () => {

        let userAdmin, work, client;

        before(tryCatch(async () => {
            userAdmin = await Helpers.tests.logIn(agent);
            work = await Helpers.tests.getRandomWork();
            client = await Helpers.tests.getRandomClient();
        }));

        describe.only('test work skills', () => {

            let workSkill, skill;

            beforeEach(tryCatch(async () => {
                workSkill = await Helpers.tests.getRandomWorkSkill();
                skill = await Helpers.tests.getRandomSkill();
            }));

            describe.only('route /:work_id/skills/:work_skill_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    agent.delete(`${basePath}/${workSkill.work_id}/skills/${workSkill.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:work_id/skills', () => {

                it.only(HttpMethods.GET, done => {

                    agent.get(`${basePath}/${work.id}/skills`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    agent.post(`${basePath}/${work.id}/skills`)
                        .send({skill_id: skill.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(201);
                            expect(res.body).to.have.property('data').to.be.an('object');
                            expect(res.body.data).to.have.property('work_id').to.equals(work.id);
                            expect(res.body.data).to.have.property('skill_id').to.equals(skill.id);
                            done();
                        });
                });
            });
        });

        describe.only('test work tags', () => {

            let workTag, tag;

            beforeEach(tryCatch(async () => {
                workTag = await Helpers.tests.getRandomWorkTag();
                tag = await Helpers.tests.getRandomTag();
            }));

            describe.only('route /:work_id/tags/:work_tag_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    agent.delete(`${basePath}/${workTag.work_id}/tags/${workTag.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:work_id/tags', () => {

                it.only(HttpMethods.GET, done => {

                    agent.get(`${basePath}/${work.id}/tags`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    agent.post(`${basePath}/${work.id}/tags`)
                        .send({tag_id: tag.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(201);
                            expect(res.body).to.have.property('data').to.be.an('object');
                            expect(res.body.data).to.have.property('work_id').to.equals(work.id);
                            expect(res.body.data).to.have.property('tag_id').to.equals(tag.id);
                            done();
                        });
                });
            });
        });

        describe.only('test work shares', () => {

            let workShare, share;

            beforeEach(tryCatch(async () => {
                workShare = await Helpers.tests.getRandomWorkShare();
                share = await Helpers.tests.getRandomShare();
            }));

            describe.only('route /:work_id/shares/:work_share_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    agent.delete(`${basePath}/${workShare.work_id}/shares/${workShare.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:work_id/shares', () => {

                it.only(HttpMethods.GET, done => {

                    agent.get(`${basePath}/${work.id}/shares`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    agent.post(`${basePath}/${work.id}/shares`)
                        .send({share_id: share.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(201);
                            expect(res.body).to.have.property('data').to.be.an('object');
                            expect(res.body.data).to.have.property('work_id').to.equals(work.id);
                            expect(res.body.data).to.have.property('share_id').to.equals(share.id);
                            done();
                        });
                });
            });
        });

        describe.only('test work categories', () => {

            let workCategory, category;

            beforeEach(tryCatch(async () => {
                workCategory = await Helpers.tests.getRandomWorkCategory();
                category = await Helpers.tests.getRandomCategory();
            }));

            describe.only('route /:work_id/categories/:work_category_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    agent.delete(`${basePath}/${workCategory.work_id}/categories/${workCategory.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:work_id/categories', () => {

                it.only(HttpMethods.GET, done => {

                    agent.get(`${basePath}/${work.id}/categories`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    agent.post(`${basePath}/${work.id}/categories`)
                        .send({category_id: category.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(201);
                            expect(res.body).to.have.property('data').to.be.an('object');
                            expect(res.body.data).to.have.property('work_id').to.equals(work.id);
                            expect(res.body.data).to.have.property('category_id').to.equals(category.id);
                            done();
                        });
                });
            });
        });

        describe.only('test work comments', () => {

            let workComment;

            beforeEach(tryCatch(async () => {
                workComment = await Helpers.tests.getRandomWorkComment();
            }));

            describe.only('route /:work_id/comments/:work_comment_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    agent.delete(`${basePath}/${workComment.work_id}/comments/${workComment.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:work_id/comments', () => {

                it.only(HttpMethods.GET, done => {

                    agent.get(`${basePath}/${work.id}/comments`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    const message = faker.lorem.sentence();

                    agent.post(`${basePath}/${work.id}/comments`)
                        .send({work_comment: message})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(201);
                            expect(res.body).to.have.property('data').to.be.an('object');
                            expect(res.body.data).to.have.property('work_id').to.equals(work.id);
                            expect(res.body.data).to.have.property('work_comment').to.equals(message);
                            expect(res.body.data).to.have.property('work_comment_user').to.equals(userAdmin.user_id);
                            done();
                        });
                });
            });
        });

        describe.only('route /works/:work_id', () => {

            it.only(HttpMethods.GET, done => {

                agent.get(`${basePath}/${work.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {

                const work_name = faker.lorem.sentence(),
                    work_client = client.id,
                    work_desc = faker.lorem.sentence(),
                    work_url = faker.lorem.sentence(),
                    work_d_public = moment().toISOString(),
                    work_site_url = faker.lorem.sentence();

                agent.patch(`${basePath}/${work.id}`)
                    .send({work_name, work_client, work_url, work_d_public, work_site_url, work_desc})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('work_name').to.equal(work_name);
                        expect(res.body.data).to.have.property('work_desc').to.equal(work_desc);
                        expect(res.body.data).to.have.property('work_client').to.equal(work_client);
                        expect(res.body.data).to.have.property('work_url').to.equal(work_url);
                        expect(res.body.data).to.have.property('work_d_public').to.equal(work_d_public);
                        expect(res.body.data).to.have.property('work_site_url').to.equal(work_site_url);
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {

                agent.delete(`${basePath}/${work.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /works', () => {

            it.only(HttpMethods.GET, done => {

                agent.get(basePath)
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('count').to.be.a('number');
                        expect(res.body.data).to.have.property('rows').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {

                const work_name = faker.lorem.sentence(),
                    work_client = client.id,
                    work_desc = faker.lorem.sentence(),
                    work_url = faker.lorem.sentence(),
                    work_d_public = moment().toISOString(),
                    work_site_url = faker.lorem.sentence();

                agent.post(basePath)
                    .send({work_name, work_client, work_url, work_d_public, work_site_url, work_desc})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(201);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('work_name').to.equal(work_name);
                        expect(res.body.data).to.have.property('work_desc').to.equal(work_desc);
                        expect(res.body.data).to.have.property('work_client').to.equal(work_client);
                        expect(res.body.data).to.have.property('work_url').to.equal(work_url);
                        expect(res.body.data).to.have.property('work_d_public').to.equal(work_d_public);
                        expect(res.body.data).to.have.property('work_site_url').to.equal(work_site_url);
                        done();
                    });
            });
        });
    });

    describe.only('Test user login out', () => {

        let work, client;

        before(tryCatch(async () => {
            work = await Helpers.tests.getRandomWork();
            client = await Helpers.tests.getRandomClient();
        }));

        describe.only('test work skills', () => {

            let workSkill, skill;

            beforeEach(tryCatch(async () => {
                workSkill = await Helpers.tests.getRandomWorkSkill();
                skill = await Helpers.tests.getRandomSkill();
            }));

            describe.only('route /:work_id/skills/:work_skill_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    chai.request(serverHttp).delete(`${basePath}/${workSkill.work_id}/skills/${workSkill.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:work_id/skills', () => {

                it.only(HttpMethods.GET, done => {

                    chai.request(serverHttp).get(`${basePath}/${work.id}/skills`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    chai.request(serverHttp).post(`${basePath}/${work.id}/skills`)
                        .send({skill_id: skill.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.redirects.length).to.equal(1);
                            expect(res.redirects[0]).to.have.string('/auth/login');
                            done();
                        });
                });
            });
        });

        describe.only('test work tags', () => {

            let workTag, tag;

            beforeEach(tryCatch(async () => {
                workTag = await Helpers.tests.getRandomWorkTag();
                tag = await Helpers.tests.getRandomTag();
            }));

            describe.only('route /:work_id/tags/:work_tag_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    chai.request(serverHttp).delete(`${basePath}/${workTag.work_id}/tags/${workTag.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:work_id/tags', () => {

                it.only(HttpMethods.GET, done => {

                    chai.request(serverHttp).get(`${basePath}/${work.id}/tags`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    chai.request(serverHttp).post(`${basePath}/${work.id}/tags`)
                        .send({tag_id: tag.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.redirects.length).to.equal(1);
                            expect(res.redirects[0]).to.have.string('/auth/login');
                            done();
                        });
                });
            });
        });

        describe.only('test work shares', () => {

            let workShare, share;

            beforeEach(tryCatch(async () => {
                workShare = await Helpers.tests.getRandomWorkShare();
                share = await Helpers.tests.getRandomShare();
            }));

            describe.only('route /:work_id/shares/:work_share_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    chai.request(serverHttp).delete(`${basePath}/${workShare.work_id}/shares/${workShare.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:work_id/shares', () => {

                it.only(HttpMethods.GET, done => {

                    chai.request(serverHttp).get(`${basePath}/${work.id}/shares`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    chai.request(serverHttp).post(`${basePath}/${work.id}/shares`)
                        .send({share_id: share.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.redirects.length).to.equal(1);
                            expect(res.redirects[0]).to.have.string('/auth/login');
                            done();
                        });
                });
            });
        });

        describe.only('test work categories', () => {

            let workCategory, category;

            beforeEach(tryCatch(async () => {
                workCategory = await Helpers.tests.getRandomWorkCategory();
                category = await Helpers.tests.getRandomCategory();
            }));

            describe.only('route /:work_id/categories/:work_category_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    chai.request(serverHttp).delete(`${basePath}/${workCategory.work_id}/categories/${workCategory.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:work_id/categories', () => {

                it.only(HttpMethods.GET, done => {

                    chai.request(serverHttp).get(`${basePath}/${work.id}/categories`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    chai.request(serverHttp).post(`${basePath}/${work.id}/categories`)
                        .send({category_id: category.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.redirects.length).to.equal(1);
                            expect(res.redirects[0]).to.have.string('/auth/login');
                            done();
                        });
                });
            });
        });

        describe.only('test work comments', () => {

            let workComment;

            beforeEach(tryCatch(async () => {
                workComment = await Helpers.tests.getRandomWorkComment();
            }));

            describe.only('route /:work_id/comments/:work_comment_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    chai.request(serverHttp).delete(`${basePath}/${workComment.work_id}/comments/${workComment.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:work_id/comments', () => {

                it.only(HttpMethods.GET, done => {

                    chai.request(serverHttp).get(`${basePath}/${work.id}/comments`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    const message = faker.lorem.sentence();

                    chai.request(serverHttp).post(`${basePath}/${work.id}/comments`)
                        .send({work_comment: message})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.redirects.length).to.equal(1);
                            expect(res.redirects[0]).to.have.string('/auth/login');
                            done();
                        });
                });
            });
        });

        describe.only('route /works/:work_id', () => {

            it.only(HttpMethods.GET, done => {

                chai.request(serverHttp).get(`${basePath}/${work.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {

                const work_name = faker.lorem.sentence(),
                    work_client = client.id,
                    work_desc = faker.lorem.sentence(),
                    work_url = faker.lorem.sentence(),
                    work_d_public = moment().toISOString(),
                    work_site_url = faker.lorem.sentence();

                chai.request(serverHttp).patch(`${basePath}/${work.id}`)
                    .send({work_name, work_client, work_url, work_d_public, work_site_url, work_desc})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {

                chai.request(serverHttp).delete(`${basePath}/${work.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /works', () => {

            it.only(HttpMethods.GET, done => {

                chai.request(serverHttp).get(basePath)
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('count').to.be.a('number');
                        expect(res.body.data).to.have.property('rows').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {

                const work_name = faker.lorem.sentence(),
                    work_client = client.id,
                    work_desc = faker.lorem.sentence(),
                    work_url = faker.lorem.sentence(),
                    work_d_public = moment().toISOString(),
                    work_site_url = faker.lorem.sentence();

                chai.request(serverHttp).post(basePath)
                    .send({work_name, work_client, work_url, work_d_public, work_site_url, work_desc})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });
        });
    });

    after(done => {
        serverHttp.close();
        done();
    });
});