import chai from 'chai';
import chaiHttp from 'chai-http';
import Helpers from '../../../helpers';
import moment from 'moment';
import faker from 'faker';
import {serverHttp} from '../../../server';
import {tryCatch} from '../../../decorators';
import {HttpMethods} from '../../../constants';

chai.use(chaiHttp);

const basePath = '/api/v1/articles';
const expect = chai.expect;
const agent = chai.request.agent(serverHttp);

describe.only('Testing route articles', () => {

    describe.only('Test user login in', () => {

        let userAdmin, article;

        before(tryCatch(async () => {
            userAdmin = await Helpers.tests.logIn(agent);
            article = await Helpers.tests.getRandomArticle();
        }));

        describe.only('test article tags', () => {

            let articleTag, tag;

            beforeEach(tryCatch(async () => {
                articleTag = await Helpers.tests.getRandomArticleTag();
                tag = await Helpers.tests.getRandomTag();
            }));

            describe.only('route /:article_id/tags/:article_tag_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    agent.delete(`${basePath}/${articleTag.article_id}/tags/${articleTag.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:article_id/tags', () => {

                it.only(HttpMethods.GET, done => {

                    agent.get(`${basePath}/${article.id}/tags`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    agent.post(`${basePath}/${article.id}/tags`)
                        .send({tag_id: tag.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(201);
                            expect(res.body).to.have.property('data').to.be.an('object');
                            expect(res.body.data).to.have.property('article_id').to.equals(article.id);
                            expect(res.body.data).to.have.property('tag_id').to.equals(tag.id);
                            done();
                        });
                });
            });
        });

        describe.only('test article shares', () => {

            let articleShare, share;

            beforeEach(tryCatch(async () => {
                articleShare = await Helpers.tests.getRandomArticleShare();
                share = await Helpers.tests.getRandomShare();
            }));

            describe.only('route /:article_id/shares/:article_share_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    agent.delete(`${basePath}/${articleShare.article_id}/shares/${articleShare.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:article_id/shares', () => {

                it.only(HttpMethods.GET, done => {

                    agent.get(`${basePath}/${article.id}/shares`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    agent.post(`${basePath}/${article.id}/shares`)
                        .send({share_id: share.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(201);
                            expect(res.body).to.have.property('data').to.be.an('object');
                            expect(res.body.data).to.have.property('article_id').to.equals(article.id);
                            expect(res.body.data).to.have.property('share_id').to.equals(share.id);
                            done();
                        });
                });
            });
        });

        describe.only('test article categories', () => {

            let articleCategory, category;

            beforeEach(tryCatch(async () => {
                articleCategory = await Helpers.tests.getRandomArticleCategory();
                category = await Helpers.tests.getRandomCategory();
            }));

            describe.only('route /:article_id/categories/:article_category_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    agent.delete(`${basePath}/${articleCategory.article_id}/categories/${articleCategory.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:article_id/categories', () => {

                it.only(HttpMethods.GET, done => {

                    agent.get(`${basePath}/${article.id}/categories`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    agent.post(`${basePath}/${article.id}/categories`)
                        .send({category_id: category.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(201);
                            expect(res.body).to.have.property('data').to.be.an('object');
                            expect(res.body.data).to.have.property('article_id').to.equals(article.id);
                            expect(res.body.data).to.have.property('category_id').to.equals(category.id);
                            done();
                        });
                });
            });
        });

        describe.only('test article comments', () => {

            let articleComment;

            beforeEach(tryCatch(async () => {
                articleComment = await Helpers.tests.getRandomArticleComment();
            }));

            describe.only('route /:article_id/comments/:article_comment_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    agent.delete(`${basePath}/${articleComment.article_id}/comments/${articleComment.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:article_id/comments', () => {

                it.only(HttpMethods.GET, done => {

                    agent.get(`${basePath}/${article.id}/comments`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    const message = faker.lorem.sentence();

                    agent.post(`${basePath}/${article.id}/comments`)
                        .send({article_comment: message})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(201);
                            expect(res.body).to.have.property('data').to.be.an('object');
                            expect(res.body.data).to.have.property('article_id').to.equals(article.id);
                            expect(res.body.data).to.have.property('article_comment').to.equals(message);
                            expect(res.body.data).to.have.property('article_comment_user').to.equals(userAdmin.user_id);
                            done();
                        });
                });
            });
        });

        describe.only('route /articles/:article_id', () => {

            it.only(HttpMethods.GET, done => {

                agent.get(`${basePath}/${article.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {

                const article_name = faker.lorem.sentence(),
                    article_desc = faker.lorem.sentence(),
                    article_url = faker.lorem.sentence(),
                    article_d_public = moment().toISOString();

                agent.patch(`${basePath}/${article.id}`)
                    .send({article_name, article_url, article_d_public, article_desc})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('article_name').to.equal(article_name);
                        expect(res.body.data).to.have.property('article_desc').to.equal(article_desc);
                        expect(res.body.data).to.have.property('article_url').to.equal(article_url);
                        expect(res.body.data).to.have.property('article_d_public').to.equal(article_d_public);
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {

                agent.delete(`${basePath}/${article.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /articles', () => {

            it.only(HttpMethods.GET, done => {

                agent.get(basePath)
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('count').to.be.a('number');
                        expect(res.body.data).to.have.property('rows').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {

                const article_name = faker.lorem.sentence(),
                    article_desc = faker.lorem.sentence(),
                    article_url = faker.lorem.sentence(),
                    article_d_public = moment().toISOString();

                agent.post(basePath)
                    .send({article_name, article_url, article_d_public, article_desc})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(201);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('article_name').to.equal(article_name);
                        expect(res.body.data).to.have.property('article_desc').to.equal(article_desc);
                        expect(res.body.data).to.have.property('article_url').to.equal(article_url);
                        expect(res.body.data).to.have.property('article_d_public').to.equal(article_d_public);
                        done();
                    });
            });
        });
    });

    describe.only('Test user login out', () => {

        let article;

        before(tryCatch(async () => {
            article = await Helpers.tests.getRandomArticle();
        }));

        describe.only('test article tags', () => {

            let articleTag, tag;

            beforeEach(tryCatch(async () => {
                articleTag = await Helpers.tests.getRandomArticleTag();
                tag = await Helpers.tests.getRandomTag();
            }));

            describe.only('route /:article_id/tags/:article_tag_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    chai.request(serverHttp).delete(`${basePath}/${articleTag.article_id}/tags/${articleTag.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:article_id/tags', () => {

                it.only(HttpMethods.GET, done => {

                    chai.request(serverHttp).get(`${basePath}/${article.id}/tags`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    chai.request(serverHttp).post(`${basePath}/${article.id}/tags`)
                        .send({tag_id: tag.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.redirects.length).to.equal(1);
                            expect(res.redirects[0]).to.have.string('/auth/login');
                            done();
                        });
                });
            });
        });

        describe.only('test article shares', () => {

            let articleShare, share;

            beforeEach(tryCatch(async () => {
                articleShare = await Helpers.tests.getRandomArticleShare();
                share = await Helpers.tests.getRandomShare();
            }));

            describe.only('route /:article_id/shares/:article_share_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    chai.request(serverHttp).delete(`${basePath}/${articleShare.article_id}/shares/${articleShare.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:article_id/shares', () => {

                it.only(HttpMethods.GET, done => {

                    chai.request(serverHttp).get(`${basePath}/${article.id}/shares`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    chai.request(serverHttp).post(`${basePath}/${article.id}/shares`)
                        .send({share_id: share.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.redirects.length).to.equal(1);
                            expect(res.redirects[0]).to.have.string('/auth/login');
                            done();
                        });
                });
            });
        });

        describe.only('test article categories', () => {

            let articleCategory, category;

            beforeEach(tryCatch(async () => {
                articleCategory = await Helpers.tests.getRandomArticleCategory();
                category = await Helpers.tests.getRandomCategory();
            }));

            describe.only('route /:article_id/categories/:article_category_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    chai.request(serverHttp).delete(`${basePath}/${articleCategory.article_id}/categories/${articleCategory.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:article_id/categories', () => {

                it.only(HttpMethods.GET, done => {

                    chai.request(serverHttp).get(`${basePath}/${article.id}/categories`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    chai.request(serverHttp).post(`${basePath}/${article.id}/categories`)
                        .send({category_id: category.id})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.redirects.length).to.equal(1);
                            expect(res.redirects[0]).to.have.string('/auth/login');
                            done();
                        });
                });
            });
        });

        describe.only('test article comments', () => {

            let articleComment;

            beforeEach(tryCatch(async () => {
                articleComment = await Helpers.tests.getRandomArticleComment();
            }));

            describe.only('route /:article_id/comments/:article_comment_id', () => {

                it.only(HttpMethods.DELETE, done => {

                    chai.request(serverHttp).delete(`${basePath}/${articleComment.article_id}/comments/${articleComment.id}`)
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object');
                            done();
                        });
                });
            });

            describe.only('route /:article_id/comments', () => {

                it.only(HttpMethods.GET, done => {

                    chai.request(serverHttp).get(`${basePath}/${article.id}/comments`)
                        .query({offset: 0})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.body).to.have.property('data').to.be.an('array');
                            done();
                        });
                });

                it.only(HttpMethods.POST, done => {

                    const message = faker.lorem.sentence();

                    chai.request(serverHttp).post(`${basePath}/${article.id}/comments`)
                        .send({article_comment: message})
                        .end((err, res) => {
                            if (err) return done(err);
                            expect(res).to.have.status(200);
                            expect(res.redirects.length).to.equal(1);
                            expect(res.redirects[0]).to.have.string('/auth/login');
                            done();
                        });
                });
            });
        });

        describe.only('route /articles/:article_id', () => {

            it.only(HttpMethods.GET, done => {

                chai.request(serverHttp).get(`${basePath}/${article.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {

                const article_name = faker.lorem.sentence(),
                    article_desc = faker.lorem.sentence(),
                    article_url = faker.lorem.sentence(),
                    article_d_public = moment().toISOString();

                chai.request(serverHttp).patch(`${basePath}/${article.id}`)
                    .send({article_name, article_url, article_d_public, article_desc})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {

                chai.request(serverHttp).delete(`${basePath}/${article.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /articles', () => {

            it.only(HttpMethods.GET, done => {

                chai.request(serverHttp).get(basePath)
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('count').to.be.a('number');
                        expect(res.body.data).to.have.property('rows').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {

                const article_name = faker.lorem.sentence(),
                    article_desc = faker.lorem.sentence(),
                    article_url = faker.lorem.sentence(),
                    article_d_public = moment().toISOString();

                chai.request(serverHttp).post(basePath)
                    .send({article_name, article_url, article_d_public, article_desc})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });
        });
    });

    after(done => {
        serverHttp.close();
        done();
    });
});