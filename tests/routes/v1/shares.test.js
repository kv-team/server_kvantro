import chai from 'chai';
import chaiHttp from 'chai-http';
import Helpers from '../../../helpers';
import faker from 'faker';
import {serverHttp} from '../../../server';
import {tryCatch} from '../../../decorators';
import {HttpMethods} from '../../../constants';

chai.use(chaiHttp);

const basePath = '/api/v1/shares';
const expect = chai.expect;
const agent = chai.request.agent(serverHttp);

let userAdmin, randomShare;

describe.only('Testing route shares', () => {

    describe.only('Test user login in', () => {

        beforeEach(tryCatch(async () => {
            userAdmin = await Helpers.tests.logIn(agent);
            randomShare = await Helpers.tests.getRandomShare();
        }));

        describe.only('route /shares/:id', () => {

            it.only(HttpMethods.GET, done => {

                agent.get(`${basePath}/${randomShare.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {

                const share_name = faker.lorem.sentence();

                agent.patch(`${basePath}/${randomShare.id}`)
                    .send({share_name})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('share_name').to.equal(share_name);
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {

                agent.delete(`${basePath}/${randomShare.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /shares', () => {

            it.only(HttpMethods.GET, done => {

                agent.get(basePath)
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {

                const share_name = faker.lorem.sentence();

                agent.post(basePath)
                    .send({share_name})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(201);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        expect(res.body.data).to.have.property('share_name').to.equal(share_name);
                        done();
                    });
            });
        });
    });

    describe.only('Test user login out', () => {

        beforeEach(done => {
            Helpers.tests.getRandomShare().then(data => {
                randomShare = data;
                done();
            }).catch(done);
        });

        describe.only('route /shares/:id', () => {

            it.only(HttpMethods.GET, done => {

                chai.request(serverHttp)
                    .get(`${basePath}/${randomShare.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });

            it.only(HttpMethods.PATCH, done => {
                const share_name = faker.lorem.sentence();
                chai.request(serverHttp)
                    .patch(`${basePath}/${randomShare.id}`)
                    .send({share_name})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });

            it.only(HttpMethods.DELETE, done => {
                chai.request(serverHttp)
                    .delete(`${basePath}/${randomShare.id}`)
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('object');
                        done();
                    });
            });
        });

        describe.only('route /shares', () => {

            it.only(HttpMethods.GET, done => {
                chai.request(serverHttp)
                    .get(basePath)
                    .query({offset: 0})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('data').to.be.an('array');
                        done();
                    });
            });

            it.only(HttpMethods.POST, done => {
                const share_name = faker.lorem.sentence();
                chai.request(serverHttp)
                    .post(basePath)
                    .send({share_name})
                    .end((err, res) => {
                        if (err) return done(err);
                        expect(res).to.have.status(200);
                        expect(res.redirects.length).to.equal(1);
                        expect(res.redirects[0]).to.have.string('/auth/login');
                        done();
                    });
            });
        });
    });

    after(done => {
        serverHttp.close();
        done();
    });
});