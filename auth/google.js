import Helpers from '../helpers';
import passport from 'passport';
import oauth from 'passport-google-oauth2';
import {config} from '../config';
import {t} from '../db/models';

export default (core) => {

    const GoogleStrategy = oauth.Strategy;

    passport.use(new GoogleStrategy(config.accounts.google,
        async (request, accessToken, refreshToken, profile, done) => {
            const _t = await t.startTransactionRepeatable();
            try {
                const user = await Helpers.users.getOrAdd({
                    user_name: profile.name.givenName,
                    user_is_active: true,
                    user_email: profile.email,
                    user_social: {google: {id: profile.id}}
                }, _t);
                await _t.commit();
                return done(null, user);
            } catch (err) {
                await _t.rollback();
                return done(err, false);
            }
        }
    ));

    core.app.get('/auth/google', passport.authenticate('google', {scope: ['email']}));

    core.app.get('/auth/google/callback', passport.authenticate('google', {
            successRedirect: '/',
            failureFlash: false
        })
    );
};
