import {config} from '../config';
import passport from 'passport';
import Helpers from '../helpers';
import passportVkontakte from 'passport-vkontakte';
import {t} from '../db/models';

export default (core) => {

    const VKontakteStrategy = passportVkontakte.Strategy;

    passport.use(new VKontakteStrategy(config.accounts.vk,
        async (accessToken, refreshToken, params, profile, done) => {
            const _t = await t.startTransactionRepeatable();

            try {
                const user = await Helpers.users.getOrAdd({
                    user_is_active: true,
                    user_social: {vkontakte: {id: profile.id}}
                }, _t);
                await _t.commit();
                return done(null, user);
            } catch (err) {
                await _t.rollback();
                return done(err, false);
            }
        }
    ));

    core.app.get('/auth/vk', passport.authenticate('vkontakte', {scope: ['email']}));

    core.app.get('/auth/vk/callback', passport.authenticate('vkontakte', {
        successRedirect: '/',
        failureFlash: false
    }));
};