import Helpers from '../helpers';
import passport from 'passport';
import {t} from '../db/models';
import passportLocal from 'passport-local';
import passportJWT from 'passport-jwt';
import {ConfirmSign} from '../db/enumerations';
import {tryCatch, validate} from '../decorators';

export default function (core) {

    const ExtractJWT = passportJWT.ExtractJwt;

    const LocalStrategy = passportLocal.Strategy;
    const JWTStrategy = passportJWT.Strategy;

    passport.use(new LocalStrategy(tryCatch(async (username, password, done) => {
            let user = await Helpers.users.getAuthOne({user_email: username});
            ((!user) || (!user.verifyPassword(password))) ? done({
                status: 401,
                message: ('Invalidate password or login')
            }, false) : done(null, user);
        }
    )));

    passport.use(new JWTStrategy({
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
            secretOrKey: core.config.accounts.jwt.secretKey
        }, async (jwtPayload, done) => {
            let user = await Helpers.users.getAuthOne({user_social: {local:{jwt: jwtPayload.id}}});
            ((!user) || (!user.verifyPassword(password))) ? done({
                status: 401,
                message: ('Invalidate password or login')
            }, false) : done(null, user);
        }
    ));

    core.app.post('/auth/local', passport.authenticate('local', {
        failWithError: true,
        failureFlash: false
    }), (req, res) => {
        res.status(200).redirect('/');
    }, (err, req, res, next) => {
        next(err);
    });

    core.app.get('/auth/local/callback', validate((req) => {
        req.checkQuery('token').notEmpty().isUUID().withMessage('Invalid token');
    }, async (req, res, next) => {

        const _t = await t.startTransactionRepeatable();

        try {
            const confirm = await Helpers.confirms.getOne({
                confirm_token: req.query.token,
                confirm_sign: ConfirmSign.ACCOUNT
            }, _t);
            if (confirm) {
                await Helpers.confirms.del({
                    confirm_user_id: confirm.confirm_user_id,
                    confirm_sign: ConfirmSign.ACCOUNT
                }, _t);

                const user = await Helpers.users.update({user_is_active: true}, {user_id: confirm.user_id}, _t);

                await _t.commit();

                req.logIn(user, (err) => {
                    if (err) throw err;
                });
            } else {
                await _t.rollback();
            }
            res.redirect('/');
        } catch (err) {
            await _t.rollback();
            if (err && err.redirect) {
                res.redirect('/')
            } else next(err);
        }
    }));
};