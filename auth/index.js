import google from './google';
import local from './local';
import vk from './vk';

export default (core) => {
    google(core);
    local(core);
    vk(core);
}