import * as log from '../services/logger';
import now from 'performance-now';
import {ErrorDB} from '../constants';

export const logger = (msg = 'log message empty', originCallback) => async (...args) => {
    let result = now();
    await originCallback.apply(originCallback, args);
    result = now() - result;
    log.warn(`${msg} (${result | 0} ms)`);
};

/** Данный метод оборачивает функцию контроллера */

export const tryCatch = (originCallback, errorCallback) => async (...args) => {
    try {
        return await originCallback.apply(originCallback, args);
    } catch (err) {
        if (errorCallback) await errorCallback.apply(errorCallback, [err, ...args]);
        if (args[2] && (args[2].name === 'next' || args[2].name === 'done')) args[2](err);
    }
};

export const validate = (checkCallback, originalCallback) => async (...args) => {
    await checkCallback.apply(checkCallback, args);
    const errors = args[0].validationErrors();
    (errors) ? args[2]({status: 400, name: ErrorDB.VALIDATION, errors: errors}) : originalCallback.apply(originalCallback, args);
};