import logger from '../services/logger';
import {Dev} from '../constants';

let Yaml = require('yamljs');

export class Config {

    constructor() {
        try {
            this.dev = process.env.dev || Dev.DEVELOPMENT;
            this._config = Yaml.load(`${__dirname}/config.yml`)[this.dev];
            this._db_config = Yaml.load(`${__dirname}/database.yml`)[this.dev];
        } catch (err) {
            logger.error(err.message);
            process.exit(-1);
        }
    }

    get isDevelop() {
        return (this.dev === Dev.DEVELOPMENT);
    };

    get isDebug() {
        return process.execArgv.join(' ').indexOf('--inspect-brk') !== -1;
    }

    get isProduction() {
        return (this.dev === Dev.PRODUCTION);
    };

    get() {
        return this._config ? this._config[arguments[0]] || null : null;
    };

    get db() {
        return this._db_config || {};
    };

    get redis() {
        return this._config.redis || {};
    }

    get services() {
        return this._config.services || {};
    }

    get mails() {
        return this.services.mails || {};
    }

    get rabbit() {
        return this._config.rabbit || {};
    }

    get statsd() {
        return this.services.statsd || {};
    }

    get captchas() {
        return this.services.captchas || {};
    }

    get accounts() {
        return this._config.accounts || {};
    }

    get lang() {
        return this._config.language || {};
    }
}


export class ApiConfig {

    constructor() {
        try {
            this._config = Yaml.load(`${__dirname}/api.yml`);
        } catch (err) {
            logger.error(err.message);
            process.exit(-1);
        }
    }

    get getVersionsApi() {
        return this._config['active_api_versions'];
    }

}

export const config = new Config();

export const configAPI = new ApiConfig();