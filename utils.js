import ProgressBar from 'progress';
import {configAPI} from './config';
import moment from 'moment/moment';
import readline from 'readline-sync';
import logger from './services/logger';
import fs from 'fs';

export const regExp = (regex_string) => {
    return function (req, res, next, id) {
        let regex = new RegExp(regex_string);
        if (regex.test(id)) {
            next();
        } else {
            next('route');
        }
    };
};

export const getPathApiVersion = (req) => {
    let apiVersion = '';
    configAPI.getVersionsApi.map((api) => {
        if (req.originalUrl.indexOf(`api/${api}/`) > 0) {
            apiVersion = api;
        }
    });
    return apiVersion;
};

export const formatResponse = (request, statusCode = '', data = null, err = null) => {

    let apiVersion = getPathApiVersion(request);

    let result = {
        timestamp: moment().format(),
        status: statusCode,
        statusMessage: getStatusMessageByCode(statusCode)
    };

    if (apiVersion) result.api = `${apiVersion}.0`;
    if (err) result.errors = [err.message ? err.message : err];
    if (data) result.data = data;

    return result;
};

export const getStatusMessageByCode = (code = '') => {
    let result;

    switch (code) {
        case 200:
            result = 'OK';
            break;
        case 201:
            result = 'Created';
            break;
        case 400:
            result = 'Bad Request';
            break;
        case 401:
            result = 'Unauthorized';
            break;
        case 403:
            result = 'Forbidden';
            break;
        case 404:
            result = 'Not Found';
            break;
        case 405:
            result = 'Method Not Allowed';
            break;
        case 500:
            result = 'Internal Server Error';
            break;
        default:
            result = '';
    }

    return result;
};

export const camelCaseToLine = (str = '') => {
    let result = str.replace(/([A-Z]+)/g, "_$1");
    result = result.slice(1);
    return result;
};

export const getFiles = function (dir, files_) {
    files_ = files_ || [];
    let files = fs.readdirSync(dir);
    for (let i in files) {
        let name = `${dir}/${files[i]}`;
        (fs.statSync(name).isDirectory()) ? getFiles(name, files_) : files_.push(name);
    }
    return files_;
};

export const hashCode = (str = '') => {
    let hash = 0, i, chr;
    if (!str.length) return hash;
    for (i = 0; i < str.length; i++) {
        chr = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0;
    }
    return hash;
};

export const getHexColor = (number = 0) => `#${((number) >>> 0).toString(16).slice(-6)}`;

export const getColorByHashString = (str = '') => getHexColor(hashCode(str));

export class Generator {

    static ProgressBar = ProgressBar;

    static generateCount = (object = '', callback) => {

        desc(`Generating ${object.replace('_', ' ')} example: gen:${object.toLowerCase()}[1]`);
        task(object.toLowerCase(), [], async (amount = 1) => {

            const FINISH = `Finished Generating ${object.replace('_', ' ')} ====================`,
                START = `\nStart Generating ${object.replace('_', ' ')} =======================`;

            let bar = Generator.createProgressBar(amount),
                _sum_count = 0;

            console.log(START.bold.magenta);

            for (let _idx = 0; _idx < amount; _idx++) {
                _sum_count++;
                try {
                    await callback();
                    bar.tick({
                        content: `Generate ${object.replace('_', ' ')} (${_idx + 1}/${amount}) created: ${_sum_count}`
                    });
                } catch (err) {
                    err.finish = FINISH.bold.magenta;
                    fail(err);
                }
            }
            console.log(FINISH.bold.magenta);
        });
    };

    static generateCountLimit = (limit = 100, object = '', callback) => {

        desc(`Generating ${object.replace('_', ' ')} example: gen:${object.toLowerCase()}[1]`);
        task(object.toLowerCase(), [], async (amount = 1) => {

            const FINISH = `Finished Generating ${object.replace('_', ' ')} ====================`,
                START = `\nStart Generating ${object.replace('_', ' ')} =======================`;

            if (amount > limit) {
                if (!readline.keyInYN(`Do you really want to run the task, limit more ${limit}?`.bold.yellow)) {
                    logger.info("Cancel task");
                    console.log("Finished Generating rooms ====================".bold.magenta);
                    complete();
                }
            }

            let bar = Generator.createProgressBar(amount),
                _sum_count = 0;

            console.log(START.bold.magenta);

            for (let _idx = 0; _idx < amount; _idx++) {
                _sum_count++;
                try {
                    await callback();
                    bar.tick({
                        content: `Generate ${object.replace('_', ' ')} (${_idx + 1}/${amount}) created: ${_sum_count}`
                    });
                } catch (err) {
                    err.finish = FINISH.bold.magenta;
                    fail(err);
                }
            }
            console.log(FINISH.bold.magenta);
        });
    };

    /**
     * Прогресс бар
     * @param amount
     * @return {*|ProgressBar}
     */
    static createProgressBar = (amount) => {
        return new Generator.ProgressBar(":content [:bar] :percent".bold.green, {
            total: Number(amount),
            width: 20
        });
    };
}
