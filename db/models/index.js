import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';
import {config} from '../../config';
import logger from '../../services/logger';

let _db = {
    models: {},
    t: {}
};
let sequelize = null;

try {
    sequelize = new Sequelize(config.db);
} catch (err) {
    logger.error(err.message);
    process.exit(-1);
}

fs.readdirSync(__dirname).filter(file => file.indexOf('.') !== 0 && file !== 'index.js').map(file => {
    const model = sequelize.import(path.join(__dirname, file));
    _db.models[model.name] = model;
});

/* Relations */
for (let model in _db.models) {
    if (_db.models[model].associate) _db.models[model].associate(_db.models);
}

_db = {..._db, sequelize, Sequelize};

_db.t = {
    startTransactionSerial: () => sequelize.transaction({isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.SERIALIZABLE}),
    startTransactionUnCommitted: () => sequelize.transaction({isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED}),
    startTransactionCommitted: () => sequelize.transaction({isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.READ_COMMITTED}),
    startTransactionRepeatable: () => sequelize.transaction({isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.REPEATABLE_READ})
};

export const models = _db.models;
export const db = _db.sequelize;
export const t = _db.t;
