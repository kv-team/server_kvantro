import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    return sequelize.define(Structure.CLIENTS, {
        client_name: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        client_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });
};
