import {Structure} from '../../constants';
import {config} from '../../config';
import {ConfirmSign} from '../enumerations';
import crypto from 'crypto';
import Mailer from '../../services/mailer';

export default (sequelize, DataTypes) => {
    const Users = sequelize.define(Structure.USERS, {
        user_name: {
            type: DataTypes.STRING(32),
            allowNull: false
        },
        user_surname: {
            type: DataTypes.STRING(32),
        },
        user_last_name: {
            type: DataTypes.STRING(32),
        },
        user_email: {
            type: DataTypes.STRING(128),
            unique: true,
            validate: {
                isEmail: true
            }
        },
        user_phone: {
            type: DataTypes.STRING,
            unique: true
        },
        user_social: {
            type: DataTypes.JSONB
        },
        user_id: {
            type: DataTypes.UUID,
            unique: true,
            allowNull: false,
            defaultValue: DataTypes.UUIDV4
        },
        user_is_active: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        user_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    /**
     * Отправка ссылки на подтверждение регистраци
     */
    Users.prototype.confirmEmail = async function (...args) {
        try {
            const confirm = await require('../../helpers').confirms.getOrAdd({
                confirm_user_id: this.user_id,
                confirm_sign: ConfirmSign.ACCOUNT
            }, args[0] || null);

            await Mailer.sendMessage({
                To: [{
                    Name: this.first_name,
                    Email: this.user_email
                }],
                TemplateID: config.mails.mailjet.templates.activeAccountID,
                Variables: {
                    confirmToken: confirm.confirm_token
                }
            });
            return confirm;
        } catch (err) {
            throw err;
        }
    };

    /**
     * Отправка ссылки на сброс пароля
     */
    Users.prototype.resetPswEmail = async function (...args) {
        try {
            let confirm = await require('../../helpers').confirms.getOrAdd({
                confirm_user_id: this.user_id,
                confirm_sign: ConfirmSign.RESTORE_PSW
            }, args[0] || null);

            await Mailer.sendMessage({
                To: [{
                    Name: this.first_name,
                    Email: this.user_email
                }],
                TemplateID: config.mails.mailjet.templates.resetPasswordID,
                Variables: {
                    confirmToken: confirm.confirm_token
                }
            });
            return confirm;
        } catch (err) {
            throw err;
        }
    };

    Users.prototype.verifyPassword = function (password = '') {
        if (this.user_social.local) {
            return this.user_social.local.psw === crypto.createHash('md5').update(password).digest('hex') ||
                this.user_social.local.psw === password;
        } else {
            return false;
        }
    };

    Users.associate = (models) => {
        Users.hasMany(models[Structure.CONFIRMATIONS], {foreignKey: 'confirm_user_id'});

        Users.addScope('all', {
            attributes: ['user_name', 'user_surname', 'user_last_name', 'user_email', 'user_phone', 'user_id'],
            where: {user_is_active: true}
        });

        Users.addScope('auth', {
            where: {user_is_active: true}
        });
    };

    return Users;
};
