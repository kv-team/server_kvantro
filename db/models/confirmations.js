import {ConfirmSign} from '../../db/enumerations';
import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const Confirmations = sequelize.define(Structure.CONFIRMATIONS, {
        confirm_user_id: {
            type: DataTypes.UUID,
            allowNull: false,
            references: {
                model: Structure.USERS,
                key: 'user_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        confirm_sign: {
            type: DataTypes.ENUM,
            allowNull: false,
            values: Object.keys(ConfirmSign)
        },
        confirm_token: {
            type: DataTypes.UUID,
            unique: true,
            allowNull: false,
            defaultValue: DataTypes.UUIDV4
        }
    }, {
        timestamps: false,
        underscored: true
    });

    Confirmations.associate = (models) => {
        Confirmations.belongsTo(models[Structure.USERS], {foreignKey: 'confirm_user_id', targetKey: 'user_id'});
    };

    return Confirmations;
};
