import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const ArticlesShares = sequelize.define(Structure.ARTICLES_SHARES, {
        share_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.SHARES,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        article_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.ARTICLES,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        article_share_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    ArticlesShares.associate = (models) => {
        ArticlesShares.belongsTo(models[Structure.SHARES], {foreignKey: 'share_id'});
    };

    return ArticlesShares;
};
