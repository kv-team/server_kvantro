import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    return sequelize.define(Structure.CATEGORIES, {
        category_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        category_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });
};
