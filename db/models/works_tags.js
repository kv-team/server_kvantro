import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const WorksTags = sequelize.define(Structure.WORKS_TAGS, {
        tag_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.TAGS,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.WORKS,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_tag_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    WorksTags.associate = (models) => {
        WorksTags.belongsTo(models[Structure.TAGS], {foreignKey: 'tag_id'});
    };

    return WorksTags;
};
