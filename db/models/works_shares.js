import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const WorksShares = sequelize.define(Structure.WORKS_SHARES, {
        share_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.SHARES,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.WORKS,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_share_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    WorksShares.associate = (models) => {
        WorksShares.belongsTo(models[Structure.SHARES], {foreignKey: 'share_id'});
    };

    return WorksShares;
};
