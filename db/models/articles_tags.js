import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const ArticlesTags = sequelize.define(Structure.ARTICLES_TAGS, {
        article_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.ARTICLES,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        tag_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.TAGS,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        article_tag_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    ArticlesTags.associate = (models) => {
        ArticlesTags.belongsTo(models[Structure.TAGS], {foreignKey: 'tag_id'});
    };

    return ArticlesTags;
};
