import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const WorksSkills = sequelize.define(Structure.WORKS_SKILLS, {
        skill_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.SKILLS,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.WORKS,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_skill_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    WorksSkills.associate = (models) => {
        WorksSkills.belongsTo(models[Structure.SKILLS], {foreignKey: 'skill_id'});
    };
    
    return WorksSkills;
};
