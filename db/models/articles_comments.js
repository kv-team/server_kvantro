import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const ArticlesComments = sequelize.define(Structure.ARTICLES_COMMENTS, {
        article_comment: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        article_comment_d_public: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        },
        article_comment_user: {
            type: DataTypes.UUID,
            allowNull: false,
            references: {
                model: Structure.USERS,
                key: 'user_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        article_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.ARTICLES,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        article_comment_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    ArticlesComments.associate = (models) => {
        ArticlesComments.belongsTo(models[Structure.USERS], {foreignKey: 'article_comment_user', targetKey: 'user_id'});

        ArticlesComments.addScope('all', {
            include: [models[Structure.USERS]],
            where: {
                article_comment_is_delete: false
            }
        });
    };

    return ArticlesComments;
};
