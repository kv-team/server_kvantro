import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const Tags = sequelize.define(Structure.TAGS, {
        tag_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        tag_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    Tags.associate = (models) => {
        Tags.hasMany(models[Structure.WORKS_TAGS], {foreignKey: 'tag_id'});
        Tags.hasMany(models[Structure.ARTICLES_TAGS], {foreignKey: 'tag_id'});
    };

    return Tags;
};
