import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const WorksComments = sequelize.define(Structure.WORKS_COMMENTS, {
        work_comment: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        work_comment_d_public: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        },
        work_comment_user: {
            type: DataTypes.UUID,
            allowNull: false,
            references: {
                model: Structure.USERS,
                key: 'user_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.WORKS,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_comment_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    WorksComments.associate = (models) => {
        WorksComments.belongsTo(models[Structure.USERS], {foreignKey: 'work_comment_user', targetKey: 'user_id'});

        WorksComments.addScope('all', {
            include: [models[Structure.USERS]],
            where: {
                work_comment_is_delete: false
            }
        });
    };

    return WorksComments;
};
