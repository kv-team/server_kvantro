import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const Shares = sequelize.define(Structure.SHARES, {
        share_name: {
            type: DataTypes.STRING(128),
            allowNull: false
        },
        share_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    Shares.associate = (models) => {
        Shares.hasMany(models[Structure.WORKS_SHARES], {foreignKey: 'share_id'});
        Shares.hasMany(models[Structure.ARTICLES_SHARES], {foreignKey: 'share_id'});
    };

    return Shares;
};
