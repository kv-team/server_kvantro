import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const ArticlesCategories = sequelize.define(Structure.ARTICLES_CATEGORIES, {
        article_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.ARTICLES,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        category_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.CATEGORIES,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        article_category_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    ArticlesCategories.associate = (models) => {
        ArticlesCategories.belongsTo(models[Structure.CATEGORIES], {foreignKey: 'category_id'});
    };
    
    return ArticlesCategories;
};
