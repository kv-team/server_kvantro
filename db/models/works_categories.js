import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const WorksCategories = sequelize.define(Structure.WORKS_CATEGORIES, {
        work_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.WORKS,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        category_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.CATEGORIES,
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_category_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    WorksCategories.associate = (models) => {
        WorksCategories.belongsTo(models[Structure.CATEGORIES], {foreignKey: 'category_id'});
    };
    
    return WorksCategories;
};
