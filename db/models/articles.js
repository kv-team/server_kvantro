import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {

    const Articles = sequelize.define(Structure.ARTICLES, {
        article_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        article_desc: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        article_url: {
            type: DataTypes.STRING(128)
        },
        article_d_public: {
            type: DataTypes.DATE,
            allowNull: false
        },
        article_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    Articles.associate = (models) => {
        Articles.hasMany(models[Structure.ARTICLES_COMMENTS], {foreignKey: 'article_id'});
        Articles.hasMany(models[Structure.ARTICLES_SHARES], {foreignKey: 'article_id'});
        Articles.hasMany(models[Structure.ARTICLES_TAGS], {foreignKey: 'article_id'});
        Articles.hasMany(models[Structure.ARTICLES_CATEGORIES], {foreignKey: 'article_id'});

        Articles.addScope('all', {
            include: [{
                model: models[Structure.ARTICLES_SHARES],
                include: [models[Structure.SHARES]]
            }, {
                model: models[Structure.ARTICLES_COMMENTS],
                include: [models[Structure.USERS]]
            }, {
                model: models[Structure.ARTICLES_TAGS],
                include: [models[Structure.TAGS]]
            }, {
                model: models[Structure.ARTICLES_CATEGORIES],
                include: [models[Structure.CATEGORIES]]
            }],
            where: {
                article_is_delete: false
            }
        });
    };

    return Articles;    
};
