import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    return sequelize.define(Structure.MESSAGES, {
        message_email: {
            type: DataTypes.STRING(128),
            allowNull: false,
            validate: {
                isEmail: true
            }
        },
        message_name: {
            type: DataTypes.STRING(64),
            allowNull: false
        },
        message_body: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        message_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });
};
