import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const Works = sequelize.define(Structure.WORKS, {
        work_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        work_client: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: Structure.CLIENTS,
                key: 'id'
            },
            onUpdate: 'CASCADE'
        },
        work_desc: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        work_url: {
            type: DataTypes.STRING(128),
            allowNull: false
        },
        work_d_public: {
            type: DataTypes.DATE,
            allowNull: false
        },
        work_site_url: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        work_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    Works.associate = (models) => {
        Works.belongsTo(models[Structure.CLIENTS], {foreignKey: 'work_client'});

        Works.hasMany(models[Structure.WORKS_TAGS], {foreignKey: 'work_id'});
        Works.hasMany(models[Structure.WORKS_SKILLS], {foreignKey: 'work_id'});
        Works.hasMany(models[Structure.WORKS_SHARES], {foreignKey: 'work_id'});
        Works.hasMany(models[Structure.WORKS_CATEGORIES], {foreignKey: 'work_id'});
        Works.hasMany(models[Structure.WORKS_COMMENTS], {foreignKey: 'work_id'});

        Works.addScope('all', {
            include: [models[Structure.CLIENTS],
                {model: models[Structure.WORKS_SKILLS], include: [models[Structure.SKILLS]]},
                {model: models[Structure.WORKS_COMMENTS], include: [models[Structure.USERS]]},
                {model: models[Structure.WORKS_SHARES], include: [models[Structure.SHARES]]},
                {model: models[Structure.WORKS_CATEGORIES], include: [models[Structure.CATEGORIES]]},
                {model: models[Structure.WORKS_TAGS], include: [models[Structure.TAGS]]}
            ],
            where: {
                work_is_delete: false
            }
        });
    };

    return Works;
};
