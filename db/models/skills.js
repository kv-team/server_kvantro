import {Structure} from '../../constants';

export default (sequelize, DataTypes) => {
    const Skills = sequelize.define(Structure.SKILLS, {
        skill_name: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        skill_desc: {
            type: DataTypes.STRING(255)
        },
        skill_percent: {
            type: DataTypes.NUMERIC,
            allowNull: false
        },
        skill_is_delete: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        timestamps: false,
        underscored: true
    });

    Skills.associate = (models) => {
        Skills.hasMany(models[Structure.WORKS_SKILLS], {foreignKey: 'skill_id'});
    };

    return Skills;
};
