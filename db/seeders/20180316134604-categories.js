module.exports = {
    up: queryInterface => queryInterface.bulkInsert('categories', [{
        category_name: "Все",
        category_is_delete: false
    }, {
        category_name: "Веб-дизайн",
        category_is_delete: false
    }, {
        category_name: "Веб-разработка",
        category_is_delete: false
    }, {
        category_name: "SEO продвижение",
        category_is_delete: false
    }, {
        category_name: "Верстка",
        category_is_delete: false
    }], {}),
    down: queryInterface => queryInterface.bulkDelete('categories', null, {})
};
