module.exports = {
    up: queryInterface => queryInterface.bulkInsert('shares', [{
        share_name: "twitter",
        share_is_delete: false
    }, {
        share_name: "facebook",
        share_is_delete: false
    }, {
        share_name: "github",
        share_is_delete: false
    }], {}),
    down: queryInterface => queryInterface.bulkDelete('shares', null, {})
};
