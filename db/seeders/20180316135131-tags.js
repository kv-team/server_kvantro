module.exports = {
    up: queryInterface => queryInterface.bulkInsert('tags', [{
        tag_name: "Docker",
        tag_is_delete: false
    }, {
        tag_name: "HTML",
        tag_is_delete: false
    }, {
        tag_name: "CSS",
        tag_is_delete: false
    }, {
        tag_name: "React",
        tag_is_delete: false
    }, {
        tag_name: "React/Redux",
        tag_is_delete: false
    }, {
        tag_name: "PostgreSQL",
        tag_is_delete: false
    }, {
        tag_name: "Node",
        tag_is_delete: false
    }, {
        tag_name: "Angular",
        tag_is_delete: false
    }, {
        tag_name: "Javascript",
        tag_is_delete: false
    }, {
        tag_name: "SVG",
        tag_is_delete: false
    }], {}),
    down: queryInterface => queryInterface.bulkDelete('tags', null, {})
};
