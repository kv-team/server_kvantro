module.exports = {
    up: queryInterface => queryInterface.bulkInsert('skills', [{
        skill_name: "Javascript (ES6)",
        skill_percent: 50,
        skill_desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus laoreet dolor metus, eu ullamcorper turpis ornare tincidunt. Vivamus tristique rhoncus enim.',
        skill_is_delete: false
    }, {
        skill_name: "Node.js",
        skill_percent: 50,
        skill_desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus laoreet dolor metus, eu ullamcorper turpis ornare tincidunt. Vivamus tristique rhoncus enim.',
        skill_is_delete: false
    }, {
        skill_name: "Delphi",
        skill_percent: 50,
        skill_desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus laoreet dolor metus, eu ullamcorper turpis ornare tincidunt. Vivamus tristique rhoncus enim.',
        skill_is_delete: false
    }, {
        skill_name: "Python 2.7",
        skill_percent: 50,
        skill_desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus laoreet dolor metus, eu ullamcorper turpis ornare tincidunt. Vivamus tristique rhoncus enim.',
        skill_is_delete: false
    }, {
        skill_name: "Java",
        skill_percent: 50,
        skill_desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus laoreet dolor metus, eu ullamcorper turpis ornare tincidunt. Vivamus tristique rhoncus enim.',
        skill_is_delete: false
    }, {
        skill_name: "React/Redux",
        skill_percent: 50,
        skill_desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus laoreet dolor metus, eu ullamcorper turpis ornare tincidunt. Vivamus tristique rhoncus enim.',
        skill_is_delete: false
    }, {
        skill_name: "Angular",
        skill_percent: 50,
        skill_desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus laoreet dolor metus, eu ullamcorper turpis ornare tincidunt. Vivamus tristique rhoncus enim.',
        skill_is_delete: false
    }, {
        skill_name: "Mocha + Chai, Jest, Sellenium",
        skill_percent: 50,
        skill_desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus laoreet dolor metus, eu ullamcorper turpis ornare tincidunt. Vivamus tristique rhoncus enim.',
        skill_is_delete: false
    }, {
        skill_name: "PostgreSQL, FireBird, MongoDB",
        skill_percent: 50,
        skill_desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus laoreet dolor metus, eu ullamcorper turpis ornare tincidunt. Vivamus tristique rhoncus enim.',
        skill_is_delete: false
    }], {}),
    down: queryInterface => queryInterface.bulkDelete('skills', null, {})
};
