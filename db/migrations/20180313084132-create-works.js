module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('works', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        work_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        work_client: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'clients',
                key: 'id'
            },
            onUpdate: 'CASCADE'
        },
        work_desc: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        work_url: {
            type: Sequelize.STRING(128),
            allowNull: false
        },
        work_d_public: {
            type: Sequelize.DATE,
            allowNull: false
        },
        work_site_url: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        work_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('works')
};