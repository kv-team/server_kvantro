module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('articles', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        article_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        article_desc: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        article_url: {
            type: Sequelize.STRING(128)
        },
        article_d_public: {
            type: Sequelize.DATE,
            allowNull: false
        },
        article_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('articles')

};