module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('skills', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        skill_name: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        skill_desc: {
            type: Sequelize.STRING(255)
        },
        skill_percent: {
            type: Sequelize.NUMERIC,
            allowNull: false
        },
        skill_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('skills')
};