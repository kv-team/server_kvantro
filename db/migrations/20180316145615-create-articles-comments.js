module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('articles_comments', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        article_comment: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        article_comment_d_public: {
            type: Sequelize.DATE,
            allowNull: false,
            defaultValue: Sequelize.NOW
        },
        article_comment_user: {
            type: Sequelize.UUID,
            allowNull: false,
            references: {
                model: 'users',
                key: 'user_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        article_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'articles',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        article_comment_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('articles_comments')
};