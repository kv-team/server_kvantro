module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('works_skills', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        skill_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'skills',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'works',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_skill_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('works_skills')
};