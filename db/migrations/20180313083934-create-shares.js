module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('shares', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        share_name: {
            type: Sequelize.STRING(128),
            allowNull: false
        },
        share_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('shares')
};