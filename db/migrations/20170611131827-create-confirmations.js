module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('confirmations', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        confirm_user_id: {
            type: Sequelize.UUID,
            allowNull: false,
            references: {
                model: 'users',
                key: 'user_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        confirm_sign: {
            type: Sequelize.ENUM,
            allowNull: false,
            values: ['RESTORE_PSW', 'ACCOUNT']
        },
        confirm_token: {
            type: Sequelize.UUID,
            unique: true,
            allowNull: false,
            defaultValue: Sequelize.UUIDV4
        }
    }),
    down: queryInterface => queryInterface.dropTable('confirmations')
};