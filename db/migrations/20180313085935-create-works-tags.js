module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('works_tags', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        tag_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'tags',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'works',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_tag_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => {
        return queryInterface.dropTable('works_tags')
    }
};