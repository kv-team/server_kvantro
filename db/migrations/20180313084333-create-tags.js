module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('tags', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        tag_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        tag_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('tags')
};