module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('clients', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        client_name: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        client_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('clients')
};