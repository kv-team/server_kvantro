module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('users', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        user_name: {
            type: Sequelize.STRING(32),
            allowNull: false
        },
        user_surname: {
            type: Sequelize.STRING(32),
        },
        user_last_name: {
            type: Sequelize.STRING(32),
        },
        user_email: {
            type: Sequelize.STRING(128),
            unique: true,
            validate: {
                isEmail: true
            }
        },
        user_phone: {
            type: Sequelize.STRING,
            unique: true
        },
        user_social: {
            type: Sequelize.JSONB
        },
        user_id: {
            type: Sequelize.UUID,
            unique: true,
            allowNull: false,
            defaultValue: Sequelize.UUIDV4
        },
        user_is_active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        user_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('users')
};