module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('articles_shares', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            share_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'shares',
                    key: 'id'
                },
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE'
            },
            article_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'articles',
                    key: 'id'
                },
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE'
            },
            article_share_is_delete: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false
            }
    }),
    down: queryInterface => queryInterface.dropTable('articles_shares')
};