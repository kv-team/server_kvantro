module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('works_categories', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        work_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'works',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        category_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'categories',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_category_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('works_categories')
};