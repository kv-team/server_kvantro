module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('works_shares', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        share_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'shares',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'works',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_share_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('works_shares')
};