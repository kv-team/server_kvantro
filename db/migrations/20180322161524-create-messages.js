module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('messages', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        message_email: {
            type: Sequelize.STRING(128),
            allowNull: false
        },
        message_name: {
            type: Sequelize.STRING(64),
            allowNull: false
        },
        message_body: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        message_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('messages')
};