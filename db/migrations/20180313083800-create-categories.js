module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('categories', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        category_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        category_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('categories')
};