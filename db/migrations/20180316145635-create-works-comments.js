module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('works_comments', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        work_comment: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        work_comment_d_public: {
            type: Sequelize.DATE,
            allowNull: false,
            defaultValue: Sequelize.NOW
        },
        work_comment_user: {
            type: Sequelize.UUID,
            allowNull: false,
            references: {
                model: 'users',
                key: 'user_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'works',
                key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        },
        work_comment_is_delete: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }),
    down: queryInterface => queryInterface.dropTable('works_comments')
};