import socket from 'socket.io';
import socketEvents from '../socket_events';

export default function (server, sessionMiddleware) {

    const io = socket(server);

    io.use((socket, next) => {
        sessionMiddleware(socket.request, socket.request.res, next);
    });

    socketEvents(io);
}