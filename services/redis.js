import connectRedis from 'connect-redis';
import {config} from '../config';
import session from 'express-session';
import redis from 'redis';
import RedisErrorHandler from '../errors/redis.error';

const RedisStore = connectRedis(session);
const client = redis.createClient(config.redis.port, config.redis.host);

client.on('error', client::RedisErrorHandler);

const sessionMiddleware = session({
    store: new RedisStore({client: client}),
    secret: 'secret',
    resave: false,
    saveUninitialized: false
});

export {client, sessionMiddleware};