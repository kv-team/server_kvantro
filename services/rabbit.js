import amqp from 'amqp';
import logger from './logger';
import {config} from '../config';

const implOpts = {
    reconnect: true,
    reconnectBackoffStrategy: 'exponential',
    reconnectBackoffTime: 500
};

export default function (callback) {

    const connection = amqp.createConnection(config.rabbit, implOpts);

    connection.on('error', (err) => {
        logger.error(err.message);
    });

    connection.on('ready', () => {
        logger.info(`Rabbit MQ server listening on port ${config.rabbit.port}`);
        callback.apply(callback, [connection]);
    });
}

