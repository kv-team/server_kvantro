import Pg from 'pg';
import child_process from 'child_process';
import {config} from '../config';
import logger from '../services/logger';
import {tryCatch} from '../decorators';

export default class DbService {

    constructor() {
        this._config = config;
        this._exec = child_process.exec;
        this._client = new Pg.Client(`pg://postgres:$postgres@${this._config.db.host}:${this._config.db.port}/postgres`);
        this._client.connect();
    }

    getDbByName = () => this._client.query(`SELECT datname FROM pg_database WHERE datname='${this._config.db.database}'`);

    createRoleDB = tryCatch(async () => {
        const roles = await this._client.query(`SELECT rolname FROM pg_roles WHERE rolname='${this._config.db.username}'`);
        if (roles && !roles.rows.length) {
            await this._client.query(`CREATE ROLE ${this._config.db.username} with password '${this._config.db.password}' LOGIN`);
            logger.info('Role is created');
        }
    });

    grantAll = tryCatch(async () => {
        const roles = await this._client.query(`SELECT rolname FROM pg_roles WHERE rolname='${this._config.db.username}'`);
        if (roles && roles.rows.length) {
            await this._client.query(`ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, UPDATE, DELETE, INSERT ON TABLES TO "${this._config.db.username}"`);
            logger.info('Rights tables granted');
            await this._client.query(`ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, UPDATE ON SEQUENCES TO "${this._config.db.username}"`);
            logger.info('Rights sequences granted');
        }
    });

    revokeAll = tryCatch(async () => {
        const roles = await this._client.query(`SELECT rolname FROM pg_roles WHERE rolname='${this._config.db.username}'`);
        if (roles && roles.rows.length) {
            await this._client.query(`ALTER DEFAULT PRIVILEGES IN SCHEMA public REVOKE ALL ON TABLES FROM "${this._config.db.username}"`);
            logger.info('Rights tables are removed');
            await this._client.query(`ALTER DEFAULT PRIVILEGES IN SCHEMA public REVOKE ALL ON SEQUENCES FROM "${this._config.db.username}"`);
            logger.info('Rights sequences are removed');
        }
    });

    terminateDB = tryCatch(async () => {
        await this._client.query(`SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '${this._config.db.database}'`);
        logger.info('Database terminate connections');
    });

    createDB = tryCatch(async () => {
        const db = await this.getDbByName();
        if (db && !db.rows.length) {
            await this._client.query(`CREATE DATABASE ${this._config.db.database}`);
            logger.info('Database is created');
        }
        this._client = new Pg.Client(`pg://postgres:$postgres@${this._config.db.host}:${this._config.db.port}/${this._config.db.database}`);
        await this._client.connect();
    });

    dropDB = tryCatch(async () => {
        await this._client.query(`DROP DATABASE IF EXISTS ${this._config.db.database}`);
        logger.info('Database is dropped');
    });

    deployDB = tryCatch(async () => {
        await this.terminateDB();
        await this.createDB();
        await this.revokeAll();
        await this.grantAll();
        await this.createRoleDB();
        await this.migrateStart();
        await this.seedStart();
    });

    recreateDB = tryCatch(async () => {
        await this.terminateDB();
        await this.dropDB();
        await this.deployDB();
    });

    migrateStart = () => new Promise(resolve => {
        try {
            this._exec(`sequelize db:migrate --env=${this._config.dev}`, (err) => {
                if (err)
                    throw err;
                logger.info('Migrate is complete');
                resolve();
            });
        } catch (err) {
            throw err;
        }
    });

    migrateRevert = () => new Promise(resolve => {
        try {
            this._exec(`sequelize db:migrate:undo:all --env=${this._config.dev}`, (err) => {
                if (err)
                    throw err;
                logger.info('Migrate is revert');
                resolve();
            });
        } catch (err) {
            throw err;
        }
    });

    seedStart = () => new Promise(resolve => {
        try {
            this._exec(`sequelize db:seed:all --env=${this._config.dev}`, (err) => {
                if (err)
                    throw err;
                logger.info('Seeds is complete');
                resolve();
            });
        } catch (err) {
            throw err;
        }
    });

    seedRevert = () => new Promise(resolve => {
        try {
            this._exec(`sequelize db:seed:undo:all --env=${this._config.dev}`, (err) => {
                if (err)
                    throw err;
                logger.info('Seeds is revert');
                resolve();
            });
        } catch (err) {
            throw err;
        }
    });
}
