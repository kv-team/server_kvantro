import child_process from 'child_process';
import {config} from '../config';
import {tryCatch} from '../decorators';

export default class AppService {

    constructor() {
        this._config = config;
        this._exec = child_process.exec;
    }

    testStart = () => tryCatch(new Promise(async resolve => {
        this._exec(`dev=${this._config.dev} mocha --compilers js:babel-register --recursive ./tests`, (err) => {
            if (err)
                throw err;
            resolve();
        });
    }));

    demonStart = () => tryCatch(new Promise(async resolve => {
        this._exec(`nodemon --env=${this._config.dev} --exec babel-node server.js`, {interactive: true}, (err,) => {
            if (err)
                throw err;
            resolve();
        });
    }));
}
