import StatsD from 'node-statsd';
import {config} from '../config';
import logger from './logger';

const stats = new StatsD(config.statsd.host, config.statsd.port);

stats.socket.on('error', (err) => {
    logger.error(`Error in socket: ${err}`);
});

export {
    stats
};