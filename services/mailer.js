import mailjet from 'node-mailjet';
import {config} from '../config';
import {MailjetError} from '../errors/mailjet.error';

const Mailjet = mailjet.connect(config.mails.mailjet.apiKey, config.mails.mailjet.secretKey, {
    url: config.mails.mailjet.url,
    version: config.mails.mailjet.api,
    perform_api_call: config.isDevelop
});

export default class Mailer {

    static sendMessage = async (email = {}) => {
        try {
            await Mailjet.post('send')
                .request({
                    Messages: [{
                        From: {
                            Email: config.mails.yandex.mail,
                            Name: config.mails.yandex.name
                        },
                        TemplateLanguage: true,
                        TemplateErrorDeliver: true,
                        TemplateErrorReporting: {
                            Email: config.mails.yandex.mail,
                            Name: "Error control"
                        },
                        ...email
                    }]
                });
        } catch (err) {
            throw new MailjetError(err);
        }
    };
};
