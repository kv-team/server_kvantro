import {models} from '../db/models';

export default class Helper {

    constructor(name_model = '', limit = 5, offset = 0) {
        this.limit = limit;
        this.offset = offset;
        this.name_model = name_model;
    };

    get model() {
        return models[this.name_model];
    }

    scope = (scope_name = '') => {
        this.scope_name = scope_name;
        return this;
    };

    findAll = (...args) => this.model.findAll(...args);

    findOne = (...args) => this.model.findOne(...args);

    findOrCreate = (...args) => this.model.findOrCreate(...args);

    create = (...args) => this.model.create(...args);

    patch = async (...args) => {
        try {
            const record = await this.model.update(...args);
            return record[1];
        } catch (err) {
            throw err;
        }
    };

    remove = async (...args) => {
        try {
            const record = await this.model.update(...args);
            return record[1];
        } catch (err) {
            throw err;
        }
    };

    destroy = (...args) => this.model.destroy(...args);

    getRecordCount = () => this.model.count();
};
