import {camelCaseToLine, getFiles} from '../utils';

let helper, Helpers = {};

getFiles(__dirname).filter(file => file.indexOf('.helpers') > 0).forEach((file) => {
    helper = require(file);
    Helpers[camelCaseToLine(helper.name.replace('Helpers', '')).toLowerCase()] = new helper();
});

export default Helpers;
