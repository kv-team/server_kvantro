import Helper from '../helper';
import {Structure} from '../../constants';

export default class ClientsHelpers extends Helper {

    constructor() {super(Structure.CLIENTS)};

    get = (...args) => this.findAll({
        where: {...args[0], client_is_delete: false},
        transaction: args[1] || null,
        limit: this.limit,
        offset: (args[2] || this.offset) * this.limit
    });

    getOne = (...args) => this.findOne({
        where: {...args[0], client_is_delete: false},
        transaction: args[1] || null
    });

    add = (...args) => this.create({...args[0]}, {transaction: args[1] || null});

    update = (...args) => this.patch({...args[0]}, {
        where: {...args[1]},
        returning: true, plain: true,
        transaction: args[2] || null
    });

    del = (...args) => this.remove({client_is_delete: true}, {
        where: {...args[0]},
        returning: true, plain: true,
        transaction: args[1] || null
    });

    forceDelete = async () => {};
};
