import request from 'request';
import {config} from '../config';

export default class CaptchaHelpers {

    /**
     * Проверка captcha
     * @param captchaResponse
     */
    verify = (captchaResponse) => new Promise(resolve => {
        request.post({
            url: config.captchas.google.url,
            form: {
                secret: config.captchas.google.secret,
                response: captchaResponse
            },
            json: true
        }, (err, response, body) => {
            if (err) throw err;
            /** Прошли валидацию */
            if (body.success) {
                resolve()
            } else {
                throw new Error();
            }
        });
    });
};
