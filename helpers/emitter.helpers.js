export default class EmitterHelpers {

    constructor(io) {
        this.io = io;
    }

    /**
     * Отправить сообщение об ошибке
     * @param eventText
     */
    sendError = (eventText) => new Promise(() => {
        this.io.sockets.emit('messages', {
            eventStatus: 'ERROR', message: eventText
        });
    });

    /**
     * Отправить сообщение о предупреждении
     * @param eventText
     */
    sendWarn = (eventText) => new Promise(() => {
        this.io.sockets.emit('messages', {
            eventStatus: 'WARN', message: eventText
        });
    });

    /**
     * Отправить сообщение о успешном выполнении
     * @param eventText
     */
    sendSuccess = (eventText) => new Promise(() => {
        this.io.sockets.emit('messages', {
            eventStatus: 'SUCCESS', message: eventText
        });
    });
};
