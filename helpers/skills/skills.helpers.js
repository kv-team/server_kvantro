import Helper from '../helper';
import {Structure} from '../../constants';

export default class SkillsHelpers extends Helper {

    constructor() {super(Structure.SKILLS)};

    get = (...args) => this.findAll({
        where: {...args[0], skill_is_delete: false},
        transaction: args[1] || null,
        limit: this.limit,
        offset: (args[2] || this.offset) * this.limit
    });

    getOne = (...args) => this.findOne({
        where: {...args[0], skill_is_delete: false},
        transaction: args[1] || null
    });

    add = (...args) => this.create({...args[0]}, {transaction: args[1] || null});

    update = (...args) => this.patch({...args[0]}, {
        where: {...args[1]},
        returning: true, plain: true,
        transaction: args[2] || null
    });

    del = (...args) => this.remove({skill_is_delete: true}, {
        where: {...args[0]},
        returning: true, plain: true,
        transaction: args[1] || null
    });

    forceDelete = (...args) => this.destroy({
        where: {skill_is_delete: true},
        transaction: args[0] || null
    });
};
