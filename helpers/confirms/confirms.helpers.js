import Helper from '../helper';
import {Structure} from '../../constants';

export default class ConfirmsHelpers extends Helper {

    constructor() {super(Structure.CONFIRMATIONS)};

    get = (...args) => this.findAll({
        where: {...args[0]},
        transaction: args[1] || null,
        limit: this.limit,
        offset: (args[2] || this.offset) * this.limit
    });

    getOrAdd = async (...args) => {
        try {
            let _data = await this.findOrCreate({
                where: {...args[0]},
                defaults: {...args[0]},
                transaction: args[1] || null
            });
            return _data[0];
        } catch (err) {
            return null;
        }
    };

    getOne = (...args) => this.findOne({
        where: {...args[0]},
        transaction: args[1] || null
    });

    add = (...args) => this.create({...args[0]}, {transaction: args[1] || null});

    update = (...args) => this.patch({...args[0]}, {
        where: {...args[1]},
        returning: true, plain: true,
        transaction: args[2] || null
    });

    del = (...args) => this.destroy({
        where: {...args[0]},
        transaction: args[1] || null
    });

    forceDelete = async () => {};
};
