import Helper from '../helper';
import {Structure} from '../../constants';

export default class UsersHelpers extends Helper {

    constructor() {super(Structure.USERS)};

    get = (...args) => this.scope('all').findAll({
        where: {...args[0], user_is_delete: false},
        transaction: args[1] || null,
        limit: this.limit,
        offset: (args[2] || this.offset) * this.limit
    });

    getOne = (...args) => this.scope('all').findOne({
        where: {...args[0], user_is_delete: false},
        transaction: args[1] || null
    });

    getAuthOne = (...args) => this.scope('auth').findOne({
        where: {...args[0], user_is_delete: false},
        transaction: args[1] || null
    });

    getOrAdd = async (...args) => {
        try {
            let _data = await this.scope('all').findOrCreate({
                where: {...args[0]},
                defaults: {...args[0]},
                transaction: args[1] || null
            });
            return _data[0];
        } catch (err) {
            throw err;
        }
    };

    add = (...args) => this.create({...args[0]}, {transaction: args[1] || null});

    update = (...args) => this.patch({...args[0]}, {
        where: {...args[1]},
        returning: true,
        plain: true,
        transaction: args[2] || null
    });

    del = (...args) => this.remove({
        user_is_delete: true,
        user_is_active: false
    }, {
        where: {...args[0]},
        returning: true,
        plain: true,
        transaction: args[1] || null
    });

    forceDelete = (...args) => this.destroy({
        where: {user_is_delete: true},
        transaction: args[0] || null
    });
};
