import crypto from 'crypto';
import faker from 'faker';
import Helpers from './index';
import {ConfirmSign} from '../db/enumerations';
import jwt from 'jsonwebtoken';
import {config} from '../config';
import {tryCatch} from '../decorators';

export default class TestsHelpers {

    static randomData = (data = []) => data[Math.floor(Math.random() * data.length)];

    addUser = tryCatch(async () => {
        const user_name = faker.name.firstName();
        const user_email = faker.internet.email();
        return await Helpers.users.add({
            user_name,
            user_surname: faker.name.firstName(),
            user_last_name: faker.name.lastName(),
            user_email,
            user_phone: faker.phone.phoneNumber(0),
            user_is_active: true,
            user_is_delete: false,
            user_social: {
                local: {
                    psw: crypto.createHash('md5').update('admin').digest('hex'),
                    jwt: jwt.sign({
                        user_name, user_email
                    }, config.accounts.jwt.secretKey)
                }
            }
        });
    });

    addCategory = tryCatch(async () => {
        return await Helpers.categories.add({
            category_name: faker.lorem.words()[0],
            category_is_delete: false
        });
    });

    addSkill = tryCatch(async () => {
        return await Helpers.skills.add({
            skill_name: faker.lorem.words()[0],
            skill_percent: faker.random.number({min: 50, max: 100}),
            skill_is_delete: false
        });
    });

    addMessage = tryCatch(async () => {
        return await Helpers.messages.add({
            message_name: faker.lorem.words()[0],
            message_email: faker.internet.email(),
            message_body: faker.lorem.sentence(),
            message_is_delete: false
        });
    });

    addShare = tryCatch(async () => {
        return await Helpers.shares.add({
            share_name: faker.lorem.words()[0],
            share_is_delete: false
        });
    });

    addTag = tryCatch(async () => {
        return await Helpers.tags.add({
            tag_name: faker.lorem.words()[0],
            tag_is_delete: false
        });
    });

    addClient = tryCatch(async () => {
        return await Helpers.clients.add({
            client_name: faker.lorem.words()[0],
            client_is_delete: false
        });
    });

    addArticle = tryCatch(async () => {
        const data = await Helpers.articles.add({
            article_name: faker.lorem.sentence(),
            article_desc: faker.lorem.sentence(),
            article_url: faker.internet.url(),
            article_d_public: new Date,
            article_is_delete: false
        });
        await Promise.all([
            this.addArticleCategory(data),
            this.addArticleTag(data),
            this.addArticleTag(data),
            this.addArticleShare(data),
            this.addArticleShare(data),
            this.addArticleComment(data),
            this.addArticleComment(data),
            this.addArticleComment(data)
        ]);
        return data;
    });

    addWork = tryCatch(async () => {
        const client = await this.getRandomClient();
        const data = await Helpers.works.add({
            work_name: faker.lorem.sentence(),
            work_client: client.id,
            work_desc: faker.lorem.sentence(),
            work_url: faker.internet.url(),
            work_d_public: new Date,
            work_site_url: faker.internet.url(),
            work_is_delete: false
        });

        await Promise.all([
            this.addWorkCategory(data),
            this.addWorkTag(data),
            this.addWorkTag(data),
            this.addWorkSkill(data),
            this.addWorkSkill(data),
            this.addWorkShare(data),
            this.addWorkShare(data),
            this.addWorkComment(data),
            this.addWorkComment(data),
            this.addWorkComment(data),
            this.addWorkComment(data)
        ]);
        return data;
    });

    addWorkTag = tryCatch(async (wk) => {
        const [tag, work] = await Promise.all([this.getRandomTag(), wk || await this.getRandomWork()]);
        return await Helpers.works_tags.add({
            tag_id: tag.id,
            work_id: work.id
        });
    });

    addWorkSkill = tryCatch(async (wk) => {
        const [skill, work] = await Promise.all([this.getRandomSkill(), wk || await this.getRandomWork()]);
        return await Helpers.works_skills.add({
            skill_id: skill.id,
            work_id: work.id
        });
    });

    addWorkComment = tryCatch(async (wk) => {
        const [user, work] = await Promise.all([this.getRandomUser(), wk || await this.getRandomWork()]);
        return await Helpers.works_comments.add({
            work_comment_user: user.user_id,
            work_comment: faker.lorem.sentence(),
            work_id: work.id
        });
    });

    addWorkShare = tryCatch(async (wk) => {
        const [share, work] = await Promise.all([this.getRandomShare(), wk || await this.getRandomWork()]);
        return await Helpers.works_shares.add({
            share_id: share.id,
            work_id: work.id
        });
    });

    addWorkCategory = tryCatch(async (wk) => {
        const [category, work] = await Promise.all([this.getRandomCategory(), wk || await this.getRandomWork()]);
        return await Helpers.works_categories.add({
            category_id: category.id,
            work_id: work.id
        });
    });

    addArticleTag = tryCatch(async (art) => {
        const [tag, article] = await Promise.all([this.getRandomTag(), art || await this.getRandomArticle()]);
        return await Helpers.articles_tags.add({
            tag_id: tag.id,
            article_id: article.id
        });
    });

    addArticleComment = tryCatch(async (art) => {
        const [user, article] = await Promise.all([this.getRandomUser(), art || await this.getRandomArticle()]);
        return await Helpers.articles_comments.add({
            article_comment_user: user.user_id,
            article_comment: faker.lorem.sentence(),
            article_id: article.id
        });
    });

    addArticleShare = tryCatch(async (art) => {
        const [share, article] = await Promise.all([this.getRandomShare(), art || await this.getRandomArticle()]);
        return await Helpers.articles_shares.add({
            share_id: share.id,
            article_id: article.id
        });
    });

    addArticleCategory = tryCatch(async (art) => {
        const [category, article] = await Promise.all([this.getRandomCategory(), art || await this.getRandomArticle()]);
        return await Helpers.articles_categories.add({
            category_id: category.id,
            article_id: article.id
        });
    });

    getRandomConfirm = tryCatch(async () => {
        const user = await this.getRandomUser();
        return await Helpers.confirms.add({
            confirm_user_id: user.user_id,
            confirm_sign: ConfirmSign.RESTORE_PSW
        });
    });

    getRandomUser = tryCatch(async () => {
        await Helpers.users.forceDelete();
        const users = await Helpers.users.get();
        return (users.length) ? TestsHelpers.randomData(users) : await this.addUser();
    });

    getRandomWorkTag = tryCatch(async () => {
        await Helpers.works_tags.forceDelete();
        const data = await Helpers.works_tags.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addWorkTag();
    });

    getRandomWorkSkill = tryCatch(async () => {
        await Helpers.works_skills.forceDelete();
        const data = await Helpers.works_skills.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addWorkSkill();
    });

    getRandomWorkShare = tryCatch(async () => {
        await Helpers.works_shares.forceDelete();
        const data = await Helpers.works_shares.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addWorkShare();
    });

    getRandomWorkComment = tryCatch(async () => {
        await Helpers.works_comments.forceDelete();
        const data = await Helpers.works_comments.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addWorkComment();
    });

    getRandomWorkCategory = tryCatch(async () => {
        await Helpers.works_categories.forceDelete();
        const data = await Helpers.works_categories.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addWorkCategory();
    });

    getRandomArticleCategory = tryCatch(async () => {
        await Helpers.articles_categories.forceDelete();
        const data = await Helpers.articles_categories.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addArticleCategory();
    });

    getRandomArticleTag = tryCatch(async () => {
        await Helpers.articles_tags.forceDelete();
        const data = await Helpers.articles_tags.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addArticleTag();
    });

    getRandomArticleShare = tryCatch(async () => {
        await Helpers.articles_shares.forceDelete();
        const data = await Helpers.articles_shares.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addArticleShare();
    });

    getRandomArticleComment = tryCatch(async () => {
        await Helpers.articles_comments.forceDelete();
        const data = await Helpers.articles_comments.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addArticleComment();
    });

    getRandomMessage = tryCatch(async () => {
        await Helpers.messages.forceDelete();
        const data = await Helpers.messages.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addMessage();
    });

    getRandomCategory = tryCatch(async () => {
        await Helpers.categories.forceDelete();
        const data = await Helpers.categories.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addCategory();
    });

    getRandomSkill = tryCatch(async () => {
        await Helpers.skills.forceDelete();
        const data = await Helpers.skills.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addSkill();
    });

    getRandomShare = tryCatch(async () => {
        await Helpers.shares.forceDelete();
        const data = await Helpers.shares.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addShare();
    });

    getRandomTag = tryCatch(async () => {
        await Helpers.tags.forceDelete();
        const data = await Helpers.tags.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addTag();
    });

    getRandomClient = tryCatch(async () => {
        await Helpers.clients.forceDelete();
        const data = await Helpers.clients.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addClient();
    });

    getRandomArticle = tryCatch(async () => {
        await Helpers.articles.forceDelete();
        const data = await Helpers.articles.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addArticle();
    });

    getRandomWork = tryCatch(async () => {
        await Helpers.works.forceDelete();
        const data = await Helpers.works.get();
        return (data.length) ? TestsHelpers.randomData(data) : await this.addWork();
    });

    logIn = async (agent) => new Promise(tryCatch(async resolve => {
        const user = await this.getRandomUser();
        agent.post('/auth/local').send({
            username: user.user_email,
            password: user.user_social.local.psw
        }).end((err) => {
            if (err) throw err;
            resolve(user);
        });
    }));
};
