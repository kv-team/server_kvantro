import Helper from '../helper';
import {Structure} from '../../constants';

export default class WorksHelpers extends Helper {

    constructor() {super(Structure.WORKS)};

    get = async (...args) => {
        const [count, rows] = await Promise.all([
            this.getRecordCount(),
            this
                .scope('all')
                .findAll({
                    where: {...args[0]},
                    transaction: args[1] || null,
                    limit: this.limit,
                    offset: (args[2] || this.offset) * this.limit
                })
        ]);
        return {count, rows};
    };

    getOne = (...args) => this.scope('all').findOne({
        where: {...args[0]},
        transaction: args[1] || null
    });

    add = (...args) => this.create({...args[0]}, {transaction: args[1] || null});

    update = (...args) => this.patch({...args[0]}, {
        where: {...args[1]},
        returning: true, plain: true,
        transaction: args[2] || null
    });

    del = (...args) => this.remove({work_is_delete: true}, {
        where: {...args[0]},
        returning: true, plain: true,
        transaction: args[1] || null
    });

    forceDelete = (...args) => this.destroy({
        where: {work_is_delete: true},
        transaction: args[0] || null
    });
};
