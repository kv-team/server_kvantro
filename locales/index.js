import i18n from 'i18n';
import {config} from '../config';

i18n.configure({
    locales: config.lang.locales,
    directory: __dirname,
    defaultLocale: config.lang.default_locale,
    cookie: 'lang'
});

export default i18n;