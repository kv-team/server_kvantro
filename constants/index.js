const Dev = {
  PRODUCTION: 'production',
  DEVELOPMENT: 'development',
  STAGING: 'staging'
};

const HttpMethods = {
  GET: 'GET',
  POST: 'POST',
  DELETE: 'DELETE',
  PATCH: 'PATCH',
  PUT: 'PUT'
};

const Error = {
    ECONNREFUSED: 'ECONNREFUSED',
    EACCESS: 'EACCESS',
    EADDRINUSE: 'EADDRINUSE',
    SIGQUIT: 'SIGQUIT',
    SIGTERM: 'SIGTERM',
    AUTHENTICATION: 'AuthenticationError'
};

const ErrorDB = {
    VALIDATION: 'SequelizeValidationError',
    FK_CONSTRAINTS: 'SequelizeForeignKeyConstraintError',
    DATABASE: 'SequelizeDatabaseError',
    UNIQUE_CONSTRAINT: 'SequelizeUniqueConstraintError'
};

const Structure = {
    ARTICLES: 'articles',
    ARTICLES_CATEGORIES: 'articles_categories',
    ARTICLES_COMMENTS: 'articles_comments',
    ARTICLES_SHARES: 'articles_shares',
    ARTICLES_TAGS: 'articles_tags',
    CATEGORIES: 'categories',
    CLIENTS: 'clients',
    CONFIRMATIONS: 'confirmations',
    MESSAGES: 'messages',
    SHARES: 'shares',
    SKILLS: 'skills',
    TAGS: 'tags',
    USERS: 'users',
    WORKS: 'works',
    WORKS_CATEGORIES: 'works_categories',
    WORKS_COMMENTS: 'works_comments',
    WORKS_SHARES: 'works_shares',
    WORKS_TAGS: 'works_tags',
    WORKS_SKILLS: 'works_skills',
};

export {
    HttpMethods,
    Dev,
    Structure,
    Error,
    ErrorDB
};