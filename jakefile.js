require('babel-register');

let jake = require('jake');
let logger = require('./services/logger');

jake.on('complete', () => {
    process.exit();
});

jake.on('error', (err) => {
    logger.error(err.message || '');
    console.log(err.finish || '');
    process.exit(1);
});

require('./utils').getFiles(__dirname).filter(file => file.indexOf('.task') > 0).forEach(file => {
    require(file);
});
