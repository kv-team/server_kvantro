import Server from './base';
import http from 'http';
import {sessionMiddleware} from '../services/redis';
import DbErrorHandler from '../errors/db.error';
import ioService from '../services/sockets';
import rabbitService from '../services/rabbit';
import routes from '../routes';
import schedulers from '../schedulers';

export default class ServerHTTP extends Server {
    constructor() {
        super();
        try {
            this._instance = http.createServer(this.app);

            /** Подключем сокеты */
            ioService(this, sessionMiddleware);
            /** Подключаем маршруты */
            routes(this);
            /** Подключаем расписание */
            schedulers(this);

            this.models.sync({force: false}).then(() => {
                this.logger.info(`Database connected`);
            }, this.models::DbErrorHandler)
                .finally(() => {

                    rabbitService((connect) => {
                        this.mq = connect;
                        this._instance.listen(this.config.get('port'), this.config.get('host'), () => {
                            this.logger.info(`HTTP server listening on port ${this.config.get('port')} and worker pid ${process.pid}`);
                        });
                    })
                });
        } catch (err) {
            throw err;
        }
    }
}