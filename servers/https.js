import Server from './base';
import https from 'https';
import fs from 'fs';
import schedulers from '../schedulers';
import {sessionMiddleware} from '../services/redis';
import ioService from '../services/sockets';
import routes from '../routes';
import DbErrorHandler from '../errors/db.error';

export default class ServerHTTPS extends Server {
    constructor() {
        super();
        try {
            this._instance = https.createServer({
                key: fs.readFileSync('key.pem'),
                cert: fs.readFileSync('cert.pem')
            }, this.app);

            /** Подключем сокеты */
            ioService(this, sessionMiddleware);
            /** Подключаем маршруты */
            routes(this);
            /** Подключаем расписание */
            schedulers(this);

            this.models.sync({force: false}).then(() => {
            }, this.models::DbErrorHandler)
                .finally(() => {
                    this._instance.listen(this.config.get('port'), this.config.get('host'), () => {
                        this.logger.info(`HTTPS server listening on port ${this.config.get('port')} and worker pid ${process.pid}`);
                    });
                });
        } catch (err) {
            throw err;
        }
    }
}