import express from 'express';
import expressWS from 'express-ws';
import pretty from 'express-prettify';
import cors from 'cors';
import favicon from 'express-favicon';
import express_logger from 'express-logger';
import expressValidator from 'express-validator';
import path from 'path';
import flash from 'connect-flash/lib/index';
import cookieParser from 'cookie-parser';
import methodOverride from 'method-override';
import * as utils from '../utils';
import bodyParser from 'body-parser';
import passport from 'passport';
import mung from 'express-mung';
import {stats} from '../services/statsd';
import logger from '../services/logger';
import {config} from '../config';
import {client, sessionMiddleware} from '../services/redis';
import {db} from '../db/models';

export default class Server {
    constructor() {
      
        this.services = {
            logger,
            stats,
            client
        };
        this._instance = null;
        this._Router = express.Router;
        this._config = config;
        this._db = db;
      
        this._app = express();
        expressWS(this._app);
        this._app.use(express.static(path.join(__dirname, 'public')));
        this._app.use(cors());
        this._app.use(pretty({query: 'pretty'}));
        this._app.use(expressValidator());
        this._app.use(favicon(path.join(__dirname, 'public/img', 'favicon.ico')));
        this._app.use(express_logger({path: 'logs/log.txt'}));
        this._app.use(mung.json((body, req, res) => utils.formatResponse(req, res.statusCode, body)));
        this._app.use(cookieParser());
        this._app.use(bodyParser.urlencoded({extended: true}));
        this._app.use(bodyParser.json());
        this._app.use(sessionMiddleware);
        this._app.use(flash());
        this._app.use(passport.initialize());
        this._app.use(passport.authenticate('session'));
        this._app.use(methodOverride());
    }

    get app() {
        return this._app;
    }

    /** Логирование на основе Winston */
    get logger() {
        return this.services.logger;
    }

    /** Сбор метрик на основе Statsd */
    get stats() {
        return this.services.stats;
    }
    
    get config() {
        return this._config;
    }

    get Router() {
        return this._Router();
    }

    get models() {
        return this._db;
    }

    get redisClient() {
        return this.services.client;
    }

    get instance() {
        return this._instance;
    }
}