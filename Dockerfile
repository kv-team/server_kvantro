FROM node:7
MAINTAINER Ivanov Alexander <mr.ivanov.aleksandr.sergeevich@gmail.com>

RUN npm install nodemon -g
RUN npm install mocha -g
RUN npm install jake -g
RUN npm install pg -g
RUN npm install sequelize-cli -g
RUN npm install babel-cli -g
RUN npm install gulp -g

RUN mkdir -p /usr/src/kvantro
COPY package.json /usr/src/kvantro/

EXPOSE 3000
EXPOSE 56745

CMD ["npm", "start"]