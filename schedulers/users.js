import Helpers from '../helpers';

export default (core, CronJob) => {

    /** Очистка записей по расписанию */
    let destroyIsDeletedUsers = new CronJob('*/2 * * * *', async () => {
        try {
            core.logger.info('Schedule: Removing all users that have a sign of removal. START');
            await Helpers.users.forceDelete();
            core.logger.info('Schedule: Removing all users that have a sign of removal. SUCCESS');
        } catch (err) {
            core.logger.error(`Schedule: Removing all users that have a sign of removal. ERROR:  ${err.message}`);
            throw err;
        }
    }, false);
    
    destroyIsDeletedUsers.start();
};
