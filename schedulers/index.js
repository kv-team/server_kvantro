import cron from 'cron-cluster';
import users from './users';

export default (core) => {

    const CronJob = cron(core.redisClient).CronJob;

    users(core, CronJob);
}