import logger from "./services/logger";

export default (io) => {

    io.sockets.on('connection', function (socket) {
        logger.info(`connection web sockets`);

        socket.on('action', () => {

        });

        socket.on('disconnect', function () {

        });
    });
};