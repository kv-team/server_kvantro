import {t} from '../db/models';

export default class Controller {

    constructor(core) {
        this._core = core;
        this._t = t;
    };
}
