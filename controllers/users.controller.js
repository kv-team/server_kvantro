import Controller from './controller';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import Helpers from '../helpers/index';
import {ConfirmSign} from '../db/enumerations/index';
import {tryCatch, validate} from '../decorators';

export default class UsersController extends Controller {

    getUsers = validate(req => {
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.users.get({}, null, req.query.offset);
        res.status(200).json(data);
    }));

    getUser = validate(req => {
        req.checkParams('user_id').notEmpty().isUUID().withMessage('Invalid user id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.users.getOne({user_id: req.params.user_id});
        res.status(200).json(data);
    }));

    addUser = validate(req => {
        if (this._core.config.isProduction) req.checkBody('g-recaptcha-response').notEmpty().isString();
        req.checkBody('psw_one').notEmpty().isString().withMessage('Password is required');
        req.checkBody('psw_two').notEmpty().equals(req.body.psw_one).withMessage('Passwords do not match');
        req.checkBody('first_name').notEmpty().isString().withMessage('Invalid first name');
        req.checkBody('email').notEmpty().isEmail().withMessage('Invalid email');
    }, tryCatch(async (req, res) => {
        const _t = await this._t.startTransactionRepeatable();
        try {
            if (this._core.config.isProduction) await Helpers.captcha.verify(req.body['g-recaptcha-response']);
            let user = await Helpers.users.getOne({user_email: req.body.email}, _t);

            if (!user) {

                user = await Helpers.users.add({
                    user_name: req.body.first_name,
                    user_email: req.body.email,
                    user_social: {
                        local: {
                            psw: crypto.createHash('md5').update(req.body.psw_one).digest('hex'),
                            jwt: jwt.sign({
                                user_name: req.body.first_name,
                                user_email: req.body.email
                            }, this._core.config.accounts.jwt.secretKey)
                        }
                    }
                }, _t);
                await user.confirmEmail(_t);
                await _t.commit();
            }
            res.status(201).json(user);
        }
        catch (err) {
            await _t.rollback();
            throw err;
        }
    }));

    patchUser = validate(req => {
        req.checkBody('username').isString().withMessage('Invalid username');
        req.checkBody('email').isEmail().withMessage('Invalid email');
    }, tryCatch(async (req, res) => {
        const _t = await this._t.startTransactionRepeatable();

        try {
            let user = await Helpers.users.getAuthOne({user_id: req.user.user_id}, _t);

            if (req.body.username) user.user_name = req.body.username;

            if (req.body.email) {
                user.user_email = req.body.email;
                user.user_is_active = (user.user_email === req.body.email);
            }

            user.user_social.local.jwt = jwt.sign({
                user_name: user.user_name,
                user_email: user.user_email
            }, this._core.config.accounts.jwt.secretKey);

            user = await Helpers.users.update(user.dataValues, {user_id: req.user.user_id}, _t);
            await _t.commit();
            res.status(200).json(user);
        } catch (err) {
            await _t.rollback();
            throw err;
        }
    }));

    delUser = tryCatch(async (req, res) => {
        await Helpers.users.del({user_id: req.user.user_id});
        res.status(200).redirect('/auth/login');
    });

    getRestoreToken = validate(req => {
        req.checkQuery('token').notEmpty().isUUID().withMessage('Invalid token');
    }, tryCatch(async (req, res) => {
        const token = await Helpers.confirms.getOne({
            confirm_token: req.query.token,
            confirm_sign: ConfirmSign.RESTORE_PSW
        });
        (token) ? res.status(200).json({token: token.confirm_token}) : res.status(302).redirect('/auth/login');
    }));

    resetPswConfirmUser = validate(req => {
        req.checkBody('token').notEmpty().isUUID().withMessage('Invalid token');
        req.checkBody('psw_one').notEmpty().isString().withMessage('Password is required');
        req.checkBody('psw_two').notEmpty().equals(req.body.psw_one).withMessage('Passwords do not match');
    }, tryCatch(async (req, res) => {
        const _t = await this._t.startTransactionRepeatable();
        try {
            const confirm = await Helpers.confirms.getOne({
                confirm_token: req.body.token,
                confirm_sign: ConfirmSign.RESTORE_PSW
            }, _t);
            const user = await Helpers.users.getOne({user_id: confirm.confirm_user_id}, _t);
            if (user) {
                let _social = {...user.user_social};
                _social.local = {..._social.local, psw: crypto.createHash('md5').update(req.body.psw_one).digest('hex')};
                await Helpers.users.update({user_social: _social}, {user_id: user.user_id}, _t);
                await Helpers.confirms.del({confirm_token: req.body.token}, _t);
            }
            await _t.commit();
            res.status(302).redirect('/auth/login');
        } catch (err) {
            await _t.rollback();
            throw err;
        }
    }));
};
