import Controller from './controller';
import Helpers from '../helpers/index';
import {tryCatch, validate} from '../decorators';

export default class AuthController extends Controller {

    isAuthorizated = (req, res, next) => {
        (req.isAuthenticated()) ? next() : res.status(302).redirect('/auth/login');
    };

    logOut = (req, res) => {
        req.logout();
        res.status(302).redirect('/auth/login');
    };

    logIn = (req, res) => {
        res.status(200);
        (req.isAuthenticated()) ? res.redirect('/') : res.json({});
    };

    resetPswEmail = validate(req => {
        req.checkBody('username').notEmpty().isString().withMessage('Invalid username');
    }, tryCatch(async (req, res) => {
        const user = await Helpers.users.getOne({user_email: req.body.username});
        if (user) await user.resetPswEmail();
        res.status(200).json({});
    }));

    serializeUser = (user, done) => done(null, user);

    deserializeUser = tryCatch(async (user, done) => {
        const data = await Helpers.users.getOne({user_id: user.user_id});
        return done(null, (data) ? data : false);
    }, async (err, user, done) => {
        return done(err, false);
    });
};
