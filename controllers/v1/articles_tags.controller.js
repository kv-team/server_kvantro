import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class ArticlesTagsController extends Controller {

    getArticleTagsByArticleId = validate(req => {
        req.checkParams('article_id').notEmpty().isInt().withMessage('Invalid article id');
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles_tags.get({article_id: req.params.article_id}, null, req.query.offset);
        res.status(200).json(data);
    }));

    addArticleTag = validate(req => {
        req.checkParams('article_id').notEmpty().isInt().withMessage('Invalid article id');
        req.checkBody('tag_id').notEmpty().isInt().withMessage('Invalid tag id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles_tags.add({article_id: req.params.article_id, tag_id: req.body.tag_id});
        res.status(201).json(data);
    }));

    delArticleTag = validate(req => {
        req.checkParams('article_tag_id').notEmpty().isInt().withMessage('Invalid tag id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles_tags.del({id: req.params.article_tag_id});
        res.status(200).json(data);
    }));
};
