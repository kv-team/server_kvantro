import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class ArticlesController extends Controller {


    getArticles = validate(req => {
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles.get({}, null, req.query.offset);
        res.status(200).json(data);
    }));

    getArticle = validate(req => {
        req.checkParams('article_id').notEmpty().isInt().withMessage('Invalid article id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles.getOne({id: req.params.article_id});
        res.status(200).json(data);
    }));

    addArticle = validate(req => {
        req.checkBody('article_name').notEmpty().isString().withMessage('Require article name');
        req.checkBody('article_desc').isString().withMessage('Require article desc');
        req.checkBody('article_url').isString().withMessage('Require article url');
        req.checkBody('article_d_public').isISO8601().withMessage('Require article public');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles.add({
            article_name: req.body.article_name,
            article_desc: req.body.article_desc,
            article_url: req.body.article_url,
            article_d_public: req.body.article_d_public
        });
        res.status(201).json(data);
    }));

    patchArticle = validate(req => {
        req.checkParams('article_id').notEmpty().isInt().withMessage('Invalid article id');
        req.checkBody('article_name').notEmpty().isString().withMessage('Require article name');
        req.checkBody('article_desc').isString().withMessage('Require article desc');
        req.checkBody('article_url').isString().withMessage('Require article url');
        req.checkBody('article_d_public').isISO8601().withMessage('Require article public');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles.update({
            article_name: req.body.article_name,
            article_desc: req.body.article_desc,
            article_url: req.body.article_url,
            article_d_public: req.body.article_d_public
        }, {id: req.params.article_id});
        res.status(200).json(data);
    }));

    delArticle = validate(req => {
        req.checkParams('article_id').notEmpty().isInt().withMessage('Invalid article id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles.del({
            id: req.params.article_id
        });
        res.status(200).json(data);
    }));
};
