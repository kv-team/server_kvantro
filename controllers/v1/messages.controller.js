import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class MessagesController extends Controller {

    getMessages = validate(req => {
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.messages.get({}, null, req.query.offset);
        res.status(200).json(data);
    }));

    getMessage = validate(req => {
        req.checkParams('message_id').notEmpty().isInt().withMessage('Invalid message id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.messages.getOne({id: req.params.message_id});
        res.status(200).json(data);
    }));

    addMessage = validate(req => {
        req.checkBody('message_name').notEmpty().isString().withMessage('Invalid message name');
        req.checkBody('message_email').notEmpty().isEmail().withMessage('Invalid message email');
        req.checkBody('message_body').notEmpty().isString().withMessage('Invalid message body');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.messages.add({
            message_name: req.body.message_name,
            message_email: req.body.message_email,
            message_body: req.body.message_body
        });
        res.status(201).json(data);
    }));

    patchMessage = validate(req => {
        req.checkParams('message_id').notEmpty().isInt().withMessage('Invalid message id');
        req.checkBody('message_name').notEmpty().isString().withMessage('Invalid message name');
        req.checkBody('message_email').notEmpty().isEmail().withMessage('Invalid message email');
        req.checkBody('message_body').notEmpty().isString().withMessage('Invalid message body');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.messages.update({
            message_name: req.body.message_name,
            message_email: req.body.message_email,
            message_body: req.body.message_body
        }, {id: req.params.message_id});
        res.status(200).json(data);
    }));

    delMessage = validate(req => {
        req.checkParams('message_id').notEmpty().isInt().withMessage('Invalid message id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.messages.del({id: req.params.message_id});
        res.status(200).json(data);
    }));
};
