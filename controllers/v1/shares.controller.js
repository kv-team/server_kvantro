import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class SharesController extends Controller {

    getShares = validate(req => {
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.shares.get({}, null, req.query.offset);
        res.status(200).json(data);
    }));

    getShare = validate(req => {
        req.checkParams('share_id').notEmpty().isInt().withMessage('Invalid share id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.shares.getOne({id: req.params.share_id});
        res.status(200).json(data);
    }));

    addShare = validate(req => {
        req.checkBody('share_name').notEmpty().isString().withMessage('Invalid share name');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.shares.add({share_name: req.body.share_name});
        res.status(201).json(data);
    }));

    patchShare = validate(req => {
        req.checkParams('share_id').notEmpty().isInt().withMessage('Invalid share id');
        req.checkBody('share_name').notEmpty().isString().withMessage('Invalid share name');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.shares.update({share_name: req.body.share_name}, {id: req.params.share_id});
        res.status(200).json(data);
    }));

    delShare = validate(req => {
        req.checkParams('share_id').notEmpty().isInt().withMessage('Invalid share id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.shares.del({id: req.params.share_id});
        res.status(200).json(data);
    }));
};
