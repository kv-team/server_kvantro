import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class WorksSkillsController extends Controller {

    getWorkSkillsByWorkId = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_skills.get({work_id: req.params.work_id}, null, req.query.offset);
        res.status(200).json(data);
    }));

    addWorkSkill = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
        req.checkBody('skill_id').notEmpty().isInt().withMessage('Invalid skill id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_skills.add({work_id: req.params.work_id, skill_id: req.body.skill_id});
        res.status(201).json(data);
    }));

    delWorkSkill = validate(req => {
        req.checkParams('work_skill_id').notEmpty().isInt().withMessage('Invalid skill id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_skills.del({id: req.params.work_skill_id});
        res.status(200).json(data);
    }));
};
