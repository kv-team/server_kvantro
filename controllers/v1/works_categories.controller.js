import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class WorksCategoriesController extends Controller {

    getWorkCategoriesByWorkId = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_categories.get({work_id: req.params.work_id}, null, req.query.offset);
        res.status(200).json(data);
    }));

    addWorkCategory = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
        req.checkBody('category_id').notEmpty().isInt().withMessage('Invalid category');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_categories.add({work_id: req.params.work_id, category_id: req.body.category_id});
        res.status(201).json(data);
    }));

    delWorkCategory = validate(req => {
        req.checkParams('work_category_id').notEmpty().isInt().withMessage('Invalid category id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_categories.del({id: req.params.work_category_id});
        res.status(200).json(data);
    }));
};
