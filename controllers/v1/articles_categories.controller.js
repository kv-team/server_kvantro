import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class ArticlesCategoriesController extends Controller {
    
    getArticleCategoriesByArticleId = validate(req => {
        req.checkParams('article_id').notEmpty().isInt().withMessage('Invalid article id');
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles_categories.get({article_id: req.params.article_id}, null, req.query.offset);
        res.status(200).json(data);
    }));

    addArticleCategory = validate(req => {
        req.checkParams('article_id').notEmpty().isInt().withMessage('Invalid article id');
        req.checkBody('category_id').notEmpty().isInt().withMessage('Invalid category id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles_categories.add({article_id: req.params.article_id, category_id: req.body.category_id});
        res.status(201).json(data);
    }));

    delArticleCategory = validate(req => {
        req.checkParams('article_category_id').notEmpty().isInt().withMessage('Invalid category id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles_categories.del({id: req.params.article_category_id});
        res.status(200).json(data);
    }));
};
