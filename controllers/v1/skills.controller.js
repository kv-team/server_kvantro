import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class SkillsController extends Controller {

    getSkills = validate(req => {
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.skills.get({}, null, req.query.offset);
        res.status(200).json(data);
    }));

    getSkill = validate(req => {
        req.checkParams('skill_id').notEmpty().isInt().withMessage('Invalid skill id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.skills.getOne({id: req.params.skill_id});
        res.status(200).json(data);
    }));

    addSkill = validate(req => {
        req.checkBody('skill_name').notEmpty().isString().withMessage('Invalid skill name');
        req.checkBody('skill_percent').notEmpty().isInt().withMessage('Invalid skill percent');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.skills.add({skill_name: req.body.skill_name, skill_percent: req.body.skill_percent});
        res.status(201).json(data);
    }));

    patchSkill = validate(req => {
        req.checkParams('skill_id').notEmpty().isInt().withMessage('Invalid skill id');
        req.checkBody('skill_name').notEmpty().isString().withMessage('Invalid skill name');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.skills.update({
            skill_name: req.body.skill_name,
            skill_percent: req.body.skill_percent
        }, {id: req.params.skill_id});
        res.status(200).json(data);
    }));

    delSkill = validate(req => {
        req.checkParams('skill_id').notEmpty().isInt().withMessage('Invalid skill id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.skills.del({id: req.params.skill_id});
        res.status(200).json(data);
    }));
};
