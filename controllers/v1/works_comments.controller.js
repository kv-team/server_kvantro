import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class WorksCommentsController extends Controller {

    getWorkCommentsByWorkId = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_comments.get({work_id: req.params.work_id}, null, req.query.offset);
        res.status(200).json(data);
    }));

    addWorkComment = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
        req.checkBody('work_comment').notEmpty().isString().withMessage('Invalid comment');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_comments.add({
            work_id: req.params.work_id,
            work_comment: req.body.work_comment,
            work_comment_user: req.user.user_id
        });
        res.status(201).json(data);
    }));

    delWorkComment = validate(req => {
        req.checkParams('work_comment_id').notEmpty().isInt().withMessage('Invalid comment id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_comments.del({id: req.params.work_comment_id});
        res.status(200).json(data);
    }));
};
