import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class CategoriesController extends Controller {

    getCategories = validate(req => {
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.categories.get({}, null, req.query.offset);
        res.status(200).json(data);
    }));

    getCategory = validate(req => {
        req.checkParams('category_id').notEmpty().isInt().withMessage('Invalid category id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.categories.getOne({id: req.params.category_id});
        res.status(200).json(data);
    }));

    addCategory = validate(req => {
        req.checkBody('category_name').notEmpty().isString().withMessage('Invalid category name');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.categories.add({category_name: req.body.category_name});
        res.status(201).json(data);
    }));

    patchCategory = validate(req => {
        req.checkParams('category_id').notEmpty().isInt().withMessage('Invalid category id');
        req.checkBody('category_name').notEmpty().isString().withMessage('Invalid category name');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.categories.update({category_name: req.body.category_name}, {id: req.params.category_id});
        res.status(200).json(data);
    }));

    delCategory = validate(req => {
        req.checkParams('category_id').notEmpty().isInt().withMessage('Invalid category id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.categories.del({id: req.params.category_id});
        res.status(200).json(data);
    }));
};
