import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class WorksSharesController extends Controller {

    getWorkSharesByWorkId = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_shares.get({work_id: req.params.work_id}, null, req.query.offset);
        res.status(200).json(data);
    }));

    addWorkShare = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
        req.checkBody('share_id').notEmpty().isInt().withMessage('Invalid share id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_shares.add({work_id: req.params.work_id, share_id: req.body.share_id});
        res.status(201).json(data);
    }));

    delWorkShare = validate(req => {
        req.checkParams('work_share_id').notEmpty().isInt().withMessage('Invalid share id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_shares.del({id: req.params.work_share_id});
        res.status(200).json(data);
    }));
};
