import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, logger, validate} from '../../decorators';

export default class WorksController extends Controller {

    getWorks = logger('Получение всех работ', validate(req => {
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works.get({}, null, req.query.offset);
        this._core.stats.increment('statsd.works.success');
        res.status(200).json(data);
    }, async () => {
        this._core.stats.increment('statsd.works.error');
    })));

    getWork = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works.getOne({id: req.params.work_id});
        res.status(200).json(data);
    }));

    addWork = validate(req => {
        req.checkBody('work_name').notEmpty().isString().withMessage('Invalid work_name');
        req.checkBody('work_client').notEmpty().isInt().withMessage('Invalid client id');
        req.checkBody('work_desc').notEmpty().isString().withMessage('Invalid work desc');
        req.checkBody('work_url').notEmpty().isString().withMessage('Invalid work url');
        req.checkBody('work_d_public').notEmpty().isISO8601().withMessage('Invalid public');
        req.checkBody('work_site_url').notEmpty().isString().withMessage('Invalid work site url');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works.add({
            work_name: req.body.work_name,
            work_client: req.body.work_client,
            work_desc: req.body.work_desc,
            work_url: req.body.work_url,
            work_d_public: req.body.work_d_public,
            work_site_url: req.body.work_site_url
        });
        res.status(201).json(data);
    }));

    patchWork = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
        req.checkBody('work_name').notEmpty().isString().withMessage('Invalid work_name');
        req.checkBody('work_client').notEmpty().isInt().withMessage('Invalid client id');
        req.checkBody('work_desc').notEmpty().isString().withMessage('Invalid work desc');
        req.checkBody('work_url').notEmpty().isString().withMessage('Invalid work url');
        req.checkBody('work_d_public').notEmpty().isISO8601().withMessage('Invalid public');
        req.checkBody('work_site_url').notEmpty().isString().withMessage('Invalid work site url');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works.update({
            work_name: req.body.work_name,
            work_client: req.body.work_client,
            work_desc: req.body.work_desc,
            work_url: req.body.work_url,
            work_d_public: req.body.work_d_public,
            work_site_url: req.body.work_site_url
        }, {id: req.params.work_id});
        res.status(200).json(data);
    }));

    delWork = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works.del({id: req.params.work_id});
        res.status(200).json(data);
    }));
};
