import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from "../../decorators";

export default class ClientsController extends Controller {

    getClients = validate(req => {
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.clients.get({}, null, req.query.offset);
        res.status(200).json(data);
    }));

    getClient = validate(req => {
        req.checkParams('client_id').notEmpty().isInt().withMessage('Invalid client id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.clients.getOne({id: req.params.client_id});
        res.status(200).json(data);
    }));

    addClient = validate(req => {
        req.checkBody('client_name').notEmpty().isString().withMessage('Invalid client name');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.clients.add({client_name: req.body.client_name});
        res.status(201).json(data);
    }));

    patchClient = validate(req => {
        req.checkParams('client_id').notEmpty().isInt().withMessage('Invalid client id');
        req.checkBody('client_name').notEmpty().isString().withMessage('Invalid client name');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.clients.update({client_name: req.body.client_name}, {id: req.params.client_id});
        res.status(200).json(data);
    }));

    delClient = validate(req => {
        req.checkParams('client_id').notEmpty().isInt().withMessage('Invalid client id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.clients.del({id: req.params.client_id});
        res.status(200).json(data);
    }));
};
