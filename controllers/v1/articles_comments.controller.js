import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class ArticlesCommentsController extends Controller {

    getArticleCommentsByArticleId = validate(req => {
        req.checkParams('article_id').notEmpty().isInt().withMessage('Invalid article id');
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles_comments.get({article_id: req.params.article_id}, null, req.query.offset);
        res.status(200).json(data);
    }));

    addArticleComment = validate(req => {
        req.checkParams('article_id').notEmpty().isInt().withMessage('Invalid article id');
        req.checkBody('article_comment').notEmpty().isString().withMessage('Invalid comment');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles_comments.add({
            article_id: req.params.article_id,
            article_comment: req.body.article_comment,
            article_comment_user: req.user.user_id
        });
        res.status(201).json(data);
    }));

    delArticleComment = validate(req => {
        req.checkParams('article_comment_id').notEmpty().isInt().withMessage('Invalid comment id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles_comments.del({id: req.params.article_comment_id});
        res.status(200).json(data);
    }));
};
