import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class TagsController extends Controller {
    
    getTags = validate(req => {
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.tags.get({}, null, req.query.offset);
        res.status(200).json(data);
    }));

    getTag = validate(req => {
        req.checkParams('tag_id').notEmpty().isInt().withMessage('Invalid tag id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.tags.getOne({id: req.params.tag_id});
        res.status(200).json(data);
    }));

    addTag = validate(req => {
        req.checkBody('tag_name').notEmpty().isString().withMessage('Invalid tag name');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.tags.add({tag_name: req.body.tag_name});
        res.status(201).json(data);
    }));

    patchTag = validate(req => {
        req.checkParams('tag_id').notEmpty().isInt().withMessage('Invalid tag id');
        req.checkBody('tag_name').notEmpty().isString().withMessage('Invalid tag name');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.tags.update({tag_name: req.body.tag_name}, {id: req.params.tag_id});
        res.status(200).json(data);
    }));

    delTag = validate(req => {
        req.checkParams('tag_id').notEmpty().isInt().withMessage('Invalid tag id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.tags.del({id: req.params.tag_id});
        res.status(200).json(data);
    }));
};
