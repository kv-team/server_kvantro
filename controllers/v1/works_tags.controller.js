import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class WorksTagsController extends Controller {

    getWorkTagsByWorkId = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_tags.get({work_id: req.params.work_id}, null, req.query.offset);
        res.status(200).json(data);
    }));

    addWorkTag = validate(req => {
        req.checkParams('work_id').notEmpty().isInt().withMessage('Invalid work id');
        req.checkBody('tag_id').notEmpty().isInt().withMessage('Invalid tag id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_tags.add({work_id: req.params.work_id, tag_id: req.body.tag_id});
        res.status(201).json(data);
    }));

    delWorkTag = validate(req => {
        req.checkParams('work_tag_id').notEmpty().isInt().withMessage('Invalid tag id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.works_tags.del({id: req.params.work_tag_id});
        res.status(200).json(data);
    }));
};
