import Controller from '../controller';
import Helpers from '../../helpers';
import {tryCatch, validate} from '../../decorators';

export default class ArticlesSharesController extends Controller {

    getArticleSharesByArticleId = validate(req => {
        req.checkParams('article_id').notEmpty().isInt().withMessage('Invalid article id');
        req.checkQuery('offset').isInt().withMessage('Invalid offset');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles_shares.get({article_id: req.params.article_id}, null, req.query.offset);
        res.status(200).json(data);
    }));

    addArticleShare = validate(req => {
        req.checkParams('article_id').notEmpty().isInt().withMessage('Invalid article id');
        req.checkBody('share_id').notEmpty().isInt().withMessage('Invalid shared id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles_shares.add({article_id: req.params.article_id, share_id: req.body.share_id});
        res.status(201).json(data);
    }));

    delArticleShare = validate(req => {
        req.checkParams('article_share_id').notEmpty().isInt().withMessage('Invalid share id');
    }, tryCatch(async (req, res) => {
        const data = await Helpers.articles_shares.del({id: req.params.article_share_id});
        res.status(200).json(data);
    }));
};
