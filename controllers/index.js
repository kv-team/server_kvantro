import {getFiles} from '../utils';
import path from 'path';
import {configAPI} from '../config';

let ctrl, Controllers = {};

/** Загружаем общие контроллеры */
getFiles(__dirname).filter(file => file.indexOf('.controller') > 0).map(file => {
    ctrl = require(file);
    Controllers[ctrl.name] = ctrl;
});

/** Загружаем версионность */
configAPI.getVersionsApi.forEach((version) => {
    Controllers[version.toLowerCase()] = {};
    getFiles(path.join(__dirname, version)).filter(file => file.indexOf('.controller') > 0).map(file => {
        ctrl = require(file);
        Controllers[version.toLowerCase()][ctrl.name] = ctrl;
    });
});

export default Controllers;
