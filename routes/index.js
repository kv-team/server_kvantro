import auth from '../auth';
import {configAPI} from '../config';
import * as utils from '../utils'
import errors from '../errors';

export default (core) => {
    core.app.get('/', (req, res) => {
        res.status(200).json({connect_status: 'SUCCESS', api_access: configAPI.getVersionsApi})
    });
    auth(core);
    configAPI.getVersionsApi.forEach((version) => {
        require(`./${version}`)(core)
    });
    core.app.use((req, res) => {
        res.status(404).json(utils.formatResponse(req, 404, null, ["API does't containts this method"]))
    });
    errors(core);
};
