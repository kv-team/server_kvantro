import Controllers from '../../controllers';

export default (core) => {

    const articlesTagsCtrl = new Controllers.v1.ArticlesTagsController(core),
        articlesSharesCtrl = new Controllers.v1.ArticlesSharesController(core),
        articlesCommentsCtrl = new Controllers.v1.ArticlesCommentsController(core),
        articlesCtrl = new Controllers.v1.ArticlesController(core),
        articlesCategoriesCtrl = new Controllers.v1.ArticlesCategoriesController(core),
        authCtrl = new Controllers.AuthController(core),
        router = core.Router;

    /* region Tags */
    router.route('/:article_id/tags/:article_tag_id')
        .delete(authCtrl.isAuthorizated, articlesTagsCtrl.delArticleTag);

    router.route('/:article_id/tags')
        .get(articlesTagsCtrl.getArticleTagsByArticleId)
        .post(authCtrl.isAuthorizated, articlesTagsCtrl.addArticleTag);
    /* endregion */

    /* region Shares */
    router.route('/:article_id/shares/:article_share_id')
        .delete(authCtrl.isAuthorizated, articlesSharesCtrl.delArticleShare);

    router.route('/:article_id/shares')
        .get(articlesSharesCtrl.getArticleSharesByArticleId)
        .post(authCtrl.isAuthorizated, articlesSharesCtrl.addArticleShare);
    /* endregion */

    /* region Comments */
    router.route('/:article_id/comments/:article_comment_id')
        .delete(authCtrl.isAuthorizated, articlesCommentsCtrl.delArticleComment);

    router.route('/:article_id/comments')
        .get(articlesCommentsCtrl.getArticleCommentsByArticleId)
        .post(authCtrl.isAuthorizated, articlesCommentsCtrl.addArticleComment);
    /* endregion */

    /* region Categories */
    router.route('/:article_id/categories/:article_category_id')
        .delete(authCtrl.isAuthorizated, articlesCategoriesCtrl.delArticleCategory);

    router.route('/:article_id/categories')
        .get(articlesCategoriesCtrl.getArticleCategoriesByArticleId)
        .post(authCtrl.isAuthorizated, articlesCategoriesCtrl.addArticleCategory);
    /* endregion */

    /* region Article */
    router.route('/:article_id')
        .get(articlesCtrl.getArticle)
        .patch(authCtrl.isAuthorizated, articlesCtrl.patchArticle)
        .delete(authCtrl.isAuthorizated, articlesCtrl.delArticle);

    router.route('/')
        .post(authCtrl.isAuthorizated, articlesCtrl.addArticle)
        .get(articlesCtrl.getArticles);
    /* endregion */

    core.app.use('/api/v1/articles', router);
};
