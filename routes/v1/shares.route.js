import Controllers from '../../controllers';

export default (core) => {

    const sharesCtrl = new Controllers.v1.SharesController(core),
        authCtrl = new Controllers.AuthController(core),
        router = core.Router;

    router.route('/:share_id')
        .get(sharesCtrl.getShare)
        .patch(authCtrl.isAuthorizated, sharesCtrl.patchShare)
        .delete(authCtrl.isAuthorizated, sharesCtrl.delShare);

    router.route('/')
        .post(authCtrl.isAuthorizated, sharesCtrl.addShare)
        .get(sharesCtrl.getShares);

    core.app.use('/api/v1/shares', router);
};
