import Controllers from '../../controllers';

export default (core) => {

    let usersCtrl = new Controllers.UsersController(core),
        authCtrl = new Controllers.AuthController(core),
        router = core.Router;

    router.route('/restore')
        .get(usersCtrl.getRestoreToken)
        .post(usersCtrl.resetPswConfirmUser);

    router.route('/:user_id')
        .get(authCtrl.isAuthorizated, usersCtrl.getUser);

    router.route('/')
        .get(authCtrl.isAuthorizated, usersCtrl.getUsers)
        .post(usersCtrl.addUser)
        .patch(authCtrl.isAuthorizated, usersCtrl.patchUser)
        .delete(authCtrl.isAuthorizated, usersCtrl.delUser);

    core.app.use('/users', router);
};
