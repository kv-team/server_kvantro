import Controllers from '../../controllers';

export default (core) => {

    const skillsCtrl = new Controllers.v1.SkillsController(core),
        authCtrl = new Controllers.AuthController(core),
        router = core.Router;

    router.route('/:skill_id')
        .get(skillsCtrl.getSkill)
        .patch(authCtrl.isAuthorizated, skillsCtrl.patchSkill)
        .delete(authCtrl.isAuthorizated, skillsCtrl.delSkill);

    router.route('/')
        .post(authCtrl.isAuthorizated, skillsCtrl.addSkill)
        .get(skillsCtrl.getSkills);

    core.app.use('/api/v1/skills', router);
};
