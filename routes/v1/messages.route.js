import Controllers from '../../controllers';

export default (core) => {

    const messagesCtrl = new Controllers.v1.MessagesController(core),
        authCtrl = new Controllers.AuthController(core),
        router = core.Router;

    router.route('/:message_id')
        .get(messagesCtrl.getMessage)
        .patch(authCtrl.isAuthorizated, messagesCtrl.patchMessage)
        .delete(authCtrl.isAuthorizated, messagesCtrl.delMessage);

    router.route('/')
        .post(authCtrl.isAuthorizated, messagesCtrl.addMessage)
        .get(messagesCtrl.getMessages);

    core.app.use('/api/v1/messages', router);
};
