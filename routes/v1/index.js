import fs from 'fs';
import path from 'path';

export default (core) => {
    fs.readdirSync(__dirname).filter(file => file.indexOf('.route') > 0).forEach(file => {
        require(path.join(__dirname, file))(core);
    });
};
