import Controllers from '../../controllers';
import passport from 'passport';

export default (core) => {

    const authCtrl = new Controllers.AuthController(core),
        router = core.Router;

    passport.serializeUser(authCtrl.serializeUser);
    passport.deserializeUser(authCtrl.deserializeUser);

    router.get('/logout', authCtrl.logOut);
    router.get('/login', authCtrl.logIn);
    router.post('/restore', authCtrl.resetPswEmail);

    core.app.use('/auth', router);
};
