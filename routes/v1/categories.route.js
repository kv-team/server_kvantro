import Controllers from '../../controllers';

export default (core) => {

    const categoriesCtrl = new Controllers.v1.CategoriesController(core),
        authCtrl = new Controllers.AuthController(core),
        router = core.Router;

    router.route('/:category_id')
        .get(categoriesCtrl.getCategory)
        .patch(authCtrl.isAuthorizated, categoriesCtrl.patchCategory)
        .delete(authCtrl.isAuthorizated, categoriesCtrl.delCategory);

    router.route('/')
        .post(authCtrl.isAuthorizated, categoriesCtrl.addCategory)
        .get(categoriesCtrl.getCategories);

    core.app.use('/api/v1/categories', router);
};
