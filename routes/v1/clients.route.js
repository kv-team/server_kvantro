import Controllers from '../../controllers';

export default (core) => {

    const clientsCtrl = new Controllers.v1.ClientsController(core),
        authCtrl = new Controllers.AuthController(core),
        router = core.Router;

    router.route('/:client_id')
        .get(clientsCtrl.getClient)
        .patch(authCtrl.isAuthorizated, clientsCtrl.patchClient)
        .delete(authCtrl.isAuthorizated, clientsCtrl.delClient);

    router.route('/')
        .post(authCtrl.isAuthorizated, clientsCtrl.addClient)
        .get(clientsCtrl.getClients);

    core.app.use('/api/v1/clients', router);
};
