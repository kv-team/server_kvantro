import Controllers from '../../controllers';

export default (core) => {

    const workTagsCtrl = new Controllers.v1.WorksTagsController(core),
        worksCtrl = new Controllers.v1.WorksController(core),
        worksSkillsCtrl = new Controllers.v1.WorksSkillsController(core),
        worksSharesCtrl = new Controllers.v1.WorksSharesController(core),
        worksCommentsCtrl = new Controllers.v1.WorksCommentsController(core),
        worksCategoriesCtrl = new Controllers.v1.WorksCategoriesController(core),
        authCtrl = new Controllers.AuthController(core),
        router = core.Router;

    /* region Skills */
    router.route('/:work_id/skills/:work_skill_id')
        .delete(authCtrl.isAuthorizated, worksSkillsCtrl.delWorkSkill);

    router.route('/:work_id/skills')
        .get(worksSkillsCtrl.getWorkSkillsByWorkId)
        .post(authCtrl.isAuthorizated, worksSkillsCtrl.addWorkSkill);
    /* endregion */

    /* region Tags */
    router.route('/:work_id/tags/:work_tag_id')
        .delete(authCtrl.isAuthorizated, workTagsCtrl.delWorkTag);

    router.route('/:work_id/tags')
        .get(workTagsCtrl.getWorkTagsByWorkId)
        .post(authCtrl.isAuthorizated, workTagsCtrl.addWorkTag);
    /* endregion */

    /* region Shares */
    router.route('/:work_id/shares/:work_share_id')
        .delete(authCtrl.isAuthorizated, worksSharesCtrl.delWorkShare);

    router.route('/:work_id/shares')
        .get(worksSharesCtrl.getWorkSharesByWorkId)
        .post(authCtrl.isAuthorizated, worksSharesCtrl.addWorkShare);
    /* endregion */

    /* region Comments */
    router.route('/:work_id/comments/:work_comment_id')
        .delete(authCtrl.isAuthorizated, worksCommentsCtrl.delWorkComment);

    router.route('/:work_id/comments')
        .get(worksCommentsCtrl.getWorkCommentsByWorkId)
        .post(authCtrl.isAuthorizated, worksCommentsCtrl.addWorkComment);
    /* endregion */

    /* region Categories */
    router.route('/:work_id/categories/:work_category_id')
        .delete(authCtrl.isAuthorizated, worksCategoriesCtrl.delWorkCategory);

    router.route('/:work_id/categories')
        .get(worksCategoriesCtrl.getWorkCategoriesByWorkId)
        .post(authCtrl.isAuthorizated, worksCategoriesCtrl.addWorkCategory);
    /* endregion */

    /* region Work */
    router.route('/:work_id')
        .get(worksCtrl.getWork)
        .patch(authCtrl.isAuthorizated, worksCtrl.patchWork)
        .delete(authCtrl.isAuthorizated, worksCtrl.delWork);

    router.route('/')
        .post(authCtrl.isAuthorizated, worksCtrl.addWork)
        .get(worksCtrl.getWorks);
    /* endregion */

    core.app.use('/api/v1/works', router);
};
