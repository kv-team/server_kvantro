import Controllers from '../../controllers';

export default (core) => {

    const tagsCtrl = new Controllers.v1.TagsController(core),
        authCtrl = new Controllers.AuthController(core),
        router = core.Router;

    router.route('/:tag_id')
        .get(tagsCtrl.getTag)
        .patch(authCtrl.isAuthorizated, tagsCtrl.patchTag)
        .delete(authCtrl.isAuthorizated, tagsCtrl.delTag);

    router.route('/')
        .post(authCtrl.isAuthorizated, tagsCtrl.addTag)
        .get(tagsCtrl.getTags);

    core.app.use('/api/v1/tags', router);
};
