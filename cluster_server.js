import cluster from 'cluster';
import os from 'os';
import {config} from './config';
import {db} from './db/models';
import logger from './services/logger';
import dbErrorHandler from './errors/db.error';

const numCPUs = config.isProduction ? os.cpus().length : process.env.WORKERS || 1;

let _portDebug = config.get('port_debug');

cluster.on('exit', (worker) => {
    logger.warn(`Worker ${worker.process.pid} exit`);
    db
        .authenticate()
        .then(() => {
            startedFork();
        })
        .catch(dbErrorHandler);
});

cluster.on('disconnect', (worker) => {
    logger.warn(`Worker ${worker.process.pid} disconnect`);
    db
        .authenticate()
        .then(() => {
            startedFork();
        }).catch(dbErrorHandler);
});

cluster.on('online', (worker) => {
});

if (cluster.isMaster) {

    cluster.setupMaster({
        execArgv: process.execArgv
    });

    for (let i = 0; i < numCPUs; ++i)
        startedFork();

} else {
    require('./server');
}

/** Запуск worker процесса */
function startedFork() {
    if (config.isDebug) cluster.settings.execArgv.push(`--inspect-brk=${_portDebug++}`);
    cluster.fork();
}